package it.postel.dao;

import static java.lang.System.exit;

import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import it.postel.bean.AuditBean;
import it.postel.bean.ClasseDocumentaleBean;
import it.postel.bean.ErroreBean;
import it.postel.bean.ReportPddClasseDocBean;
import jdk.internal.org.jline.utils.Log;
import it.postel.bean.ReportClientiFtpBean;
import it.postel.bean.MetadatiClasseDocBean;
import it.postel.bean.ParametriBean;
import it.postel.bean.ReportClientiClasseDocBean;

public class DbDao {
	static final Logger log = Logger.getLogger(DbDao.class);

	
	
	public static List<ParametriBean> getParam(Connection cn, String dbTabellaParametri) throws ClassNotFoundException, SQLException {
		//log.info("started method getParam");
		ResultSet rs = null;
		PreparedStatement ps=null;
		List<ParametriBean> paramList = null;
		ParametriBean param = null;
		try {
			String queryString = "SELECT ID_CLASSE_MODELLO,ZCODE FROM " + dbTabellaParametri + " WHERE FLAG_LAVORATO = 0 ";
			log.info("Eseguo query in method getParam: " + queryString);
			ps = cn.prepareStatement(queryString);
			rs=ps.executeQuery();
	        
			paramList = new ArrayList<ParametriBean>();
	        while(rs.next()) {
	        	param = new ParametriBean();
	        	param.setIdClasseModello(rs.getInt("ID_CLASSE_MODELLO"));
	        	param.setZcode(rs.getString("ZCODE"));
	        	paramList.add(param);
			}
			ps.close();
			rs.close();
		
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		//log.info("ended method getParam");
		return paramList;
	}
	
	
	public static ClasseDocumentaleBean getClasseDocById(Connection cn, int idClasseModello) throws SQLException {
		//log.info("started method getClasseDoc");
		ResultSet rs = null;
		PreparedStatement ps=null;
		ClasseDocumentaleBean classeDoc = new ClasseDocumentaleBean();
		
	    try {
			String queryString = "SELECT * FROM STORAGE_CLASSE_DOC WHERE ID_CLASSE_DOC= ? AND TIPO_CLASSE_DOC = 'DEFINITIVA' AND ROWNUM=1";
			log.info("Eseguo query in method getClasseDocById: " + queryString + " - ID_CLASSE_DOC " + idClasseModello);
			ps = cn.prepareStatement(queryString);
			ps.setInt(1, idClasseModello);
			rs=ps.executeQuery();
	        
			while(rs.next()) {
				classeDoc.setIdClasseDoc(rs.getObject("ID_CLASSE_DOC")!=null?rs.getInt("ID_CLASSE_DOC"):null);
				classeDoc.setNomeClasseDocumentale(rs.getString("NOME_CLASSEDOCUMENTALE"));
				classeDoc.setDescrizione(rs.getString("DESCRIZIONE"));
				classeDoc.setzCode(rs.getString("ZCODE"));
				classeDoc.setFlagAttiva(rs.getObject("FLAG_ATTIVA")!=null?rs.getBoolean("FLAG_ATTIVA"):false);
				classeDoc.setFlagEmissione(rs.getObject("FLAG_EMISSIONE")!=null?rs.getBoolean("FLAG_EMISSIONE"):false);
				classeDoc.setFlagConservazione(rs.getObject("FLAG_CONSERVAZIONE")!=null?rs.getBoolean("FLAG_CONSERVAZIONE"):false);
				classeDoc.setFlagCifrato(rs.getObject("FLAG_CIFRATO")!=null?rs.getBoolean("FLAG_CIFRATO"):false);
				classeDoc.setCtrlUnivocita(rs.getObject("CTRL_UNIVOCITA")!=null?rs.getBoolean("CTRL_UNIVOCITA"):false);
				classeDoc.setCtrlSequenzialita(rs.getObject("CTRL_SEQUENZIALITA")!=null?rs.getBoolean("CTRL_SEQUENZIALITA"):false);
				classeDoc.setFlagSezionale(rs.getObject("FLAG_SEZIONALE")!=null?rs.getBoolean("FLAG_SEZIONALE"):false);
				classeDoc.setFlagCdc(rs.getObject("FLAG_CDC")!=null?rs.getBoolean("FLAG_CDC"):false);
				classeDoc.setFlagArchiviazione(rs.getObject("FLAG_ARCHIVIAZIONE")!=null?rs.getBoolean("FLAG_ARCHIVIAZIONE"):false);
				classeDoc.setExternalHashVerifica(rs.getObject("EXTERNAL_HASH_VERIFICA")!=null?rs.getBoolean("EXTERNAL_HASH_VERIFICA"):false);
				classeDoc.setPathArchiviazione(rs.getString("PATH_ARCHIVIAZIONE"));
				classeDoc.setPathIn(rs.getString("PATH_IN"));
				classeDoc.setPathBackupPdv(rs.getString("PATH_BACKUP_PDV"));
				classeDoc.setRifPeriodoRetention(rs.getObject("RIF_PERIODO_RETENTION")!=null?rs.getInt("RIF_PERIODO_RETENTION"):null);
				classeDoc.setRifFirmatario(rs.getObject("RIF_FIRMATARIO")!=null?rs.getInt("RIF_FIRMATARIO"):null);
				classeDoc.setRifTipologia(rs.getObject("RIF_TIPOLOGIA")!=null?rs.getInt("RIF_TIPOLOGIA"):null);
				classeDoc.setRifClassDato(rs.getObject("RIF_CLASS_DATO")!=null?rs.getInt("RIF_CLASS_DATO"):null);
				classeDoc.setEstensioneDocumenti(rs.getString("ESTENSIONE_DOCUMENTI"));
				classeDoc.setRifCodiceAccounting(rs.getObject("RIF_CODICE_ACCOUNTING")!=null?rs.getInt("RIF_CODICE_ACCOUNTING"):null);
				classeDoc.setRifWorkflow(rs.getObject("RIF_WORKFLOW")!=null?rs.getInt("RIF_WORKFLOW"):null);
				classeDoc.setRifTermineConservazione(rs.getObject("RIF_TERMINE_CONSERVAZIONE")!=null?rs.getInt("RIF_TERMINE_CONSERVAZIONE"):null);
				classeDoc.setRifProcedura(rs.getObject("RIF_PROCEDURA")!=null?rs.getInt("RIF_PROCEDURA"):null);
				classeDoc.setRifFirmatarioRespCons(rs.getObject("RIF_FIRMATARIO_RESP_CONS")!=null?rs.getInt("RIF_FIRMATARIO_RESP_CONS"):null);
				classeDoc.setLivelloPriorita(rs.getObject("LIVELLO_PRIORITA")!=null?rs.getInt("LIVELLO_PRIORITA"):null);
				classeDoc.setFlagDismessa(rs.getObject("FLAG_DISMESSA")!=null?rs.getBoolean("FLAG_DISMESSA"):false);
				classeDoc.setTipoClasseDoc(rs.getString("TIPO_CLASSE_DOC")!=null?rs.getString("TIPO_CLASSE_DOC"):"DEFINITIVA");
				classeDoc.setGiorniBackupPdv(rs.getObject("GIORNI_BACKUP_PDV")!=null?rs.getInt("GIORNI_BACKUP_PDV"):null);
				classeDoc.setFlagDataCerta(rs.getObject("FLAG_DATA_CERTA")!=null?rs.getBoolean("FLAG_DATA_CERTA"):false);
				classeDoc.setFlagTimestamp(rs.getObject("FLAG_TIMESTAMP")!=null?rs.getBoolean("FLAG_TIMESTAMP"):false);
				classeDoc.setFlagConservazione(rs.getObject("FLAG_CONSERVAZIONE")!=null?rs.getBoolean("FLAG_CONSERVAZIONE"):false);
				classeDoc.setFlagTrackingGed(rs.getObject("FLAG_TRACKING_GED")!=null?rs.getBoolean("FLAG_TRACKING_GED"):false);
				classeDoc.setMeseRiferimento(rs.getObject("MESE_RIFERIMENTO")!=null?rs.getInt("MESE_RIFERIMENTO"):null);
				classeDoc.setFlagBollato(rs.getObject("FLAG_BOLLATO")!=null?rs.getBoolean("FLAG_BOLLATO"):false);
				classeDoc.setFlagSmistamento(rs.getObject("FLAG_SMISTAMENTO")!=null?rs.getBoolean("FLAG_SMISTAMENTO"):false);
				classeDoc.setRifTipologiaFirma(rs.getObject("RIF_TIPOLOGIA_FIRMA")!=null?rs.getInt("RIF_TIPOLOGIA_FIRMA"):null);
				classeDoc.setPathPrescarto(rs.getString("PATH_PRESCARTO"));
				classeDoc.setPathScarto(rs.getString("PATH_SCARTO"));
				classeDoc.setFlagSdcId(rs.getObject("FLAG_SDCID")!=null?rs.getBoolean("FLAG_SDCID"):false);
				classeDoc.setFlagControllaFirmaPdf(rs.getObject("FLAG_CONTROLLA_FIRMA_PDF")!=null?rs.getBoolean("FLAG_CONTROLLA_FIRMA_PDF"):false);
				classeDoc.setFlagNotificaOrchestratore(rs.getBoolean("FLAG_NOTIFICA_ORCHESTRATORE"));
				classeDoc.setRifModelloAgid(rs.getObject("RIF_MODELLO_AGID")!=null?rs.getInt("RIF_MODELLO_AGID"):null);
			}
			ps.close();
			rs.close();
		
		} catch (SQLException e) {
			e.printStackTrace();
			log.info("Nessuna classe trovata" );
		} 
		//log.info("ended method getClasseDoc");
		return classeDoc;
	}
	

	public static ReportClientiClasseDocBean getReportClientiByIdClasseDoc(Connection cn, Integer idClasseDoc) throws SQLException {
		//log.info("started method getReportClientiByIdClasseDoc");
		ResultSet rs = null;
		PreparedStatement ps=null;
		ReportClientiClasseDocBean repClasseDoc = null;

		try {
			String query = "SELECT * FROM STORAGE_REPORT_CLIENTI_CLASSE_DOC WHERE RIF_CLASSE_DOC=? AND ROWNUM=1";
			log.info("Eseguo query in method getReportClientiByIdClasseDoc: " + query + " - RIF_CLASSE_DOC " + idClasseDoc);
			
			ps = cn.prepareStatement(query);
			ps.setInt(1, idClasseDoc);
			rs = ps.executeQuery();
			while (rs.next()) {
					repClasseDoc = new ReportClientiClasseDocBean();
					repClasseDoc.setIdReportClienti(rs.getObject("ID_REPORT_CLIENTI")!=null?rs.getInt("ID_REPORT_CLIENTI"):null);
					repClasseDoc.setRifClasseDoc(rs.getObject("RIF_CLASSE_DOC")!=null?rs.getLong("RIF_CLASSE_DOC"):null);
					repClasseDoc.setFlagInvioRdv(rs.getObject("FLAG_INVIO_RDV")!=null?rs.getShort("FLAG_INVIO_RDV"):null);
					repClasseDoc.setFlagInvioRda(rs.getObject("FLAG_INVIO_RDA")!=null?rs.getShort("FLAG_INVIO_RDA"):null);
					repClasseDoc.setIndiciRapportoConservazione(rs.getObject("INDICI_RAPPORTO_CONSERVAZIONE")!=null?rs.getShort("INDICI_RAPPORTO_CONSERVAZIONE"):null);
					repClasseDoc.setTipoIdentificativoEsterno(rs.getObject("TIPO_IDENTIFICATIVO_ESTERNO")!=null?rs.getShort("TIPO_IDENTIFICATIVO_ESTERNO"):null);
					repClasseDoc.setTipoInvioRda(rs.getObject("TIPO_INVIO_RDA")!=null?rs.getShort("TIPO_INVIO_RDA"):null);
					repClasseDoc.setTipoInvioRdv(rs.getObject("TIPO_INVIO_RDV")!=null?rs.getShort("TIPO_INVIO_RDV"):null);
					repClasseDoc.setPathRapportoVersamento(rs.getString("PATH_RAPPORTO_VERSAMENTO"));
					repClasseDoc.setPathRapportoArchiviazione(rs.getString("PATH_RAPPORTO_ARCHIVIAZIONE"));	
			}
			ps.close();
			rs.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		//log.info("ended method getReportClientiByIdClasseDoc");
		return repClasseDoc;
	}
	

	public static ReportClientiFtpBean getReportClientiFtpByIdClasseDoc(Connection cn, Integer idClasseModello) throws SQLException {
		//log.info("started method getReportClientiFtpByIdClasseDoc");
		ResultSet rs = null;
		PreparedStatement ps=null;
		ReportClientiFtpBean repFtp= null;
		
		try {
			String query = "SELECT * FROM STORAGE_REPORT_CLIENTI_FTP WHERE RIF_CLASSE_DOC=? AND ROWNUM=1";
			log.info("Eseguo query in method getReportClientiFtpByIdClasseDoc: " + query + " - ID_CLASSE_DOC " + idClasseModello);
			
			ps = cn.prepareStatement(query);
			ps.setInt(1, idClasseModello);
			rs = ps.executeQuery();
			while (rs.next()) {		
				repFtp = new ReportClientiFtpBean();
				repFtp.setIdReportClientiFtp(rs.getObject("ID_REPORT_CLIENTI_FTP")!=null?rs.getInt("ID_REPORT_CLIENTI_FTP"):null);
				repFtp.setRifClasseDoc(rs.getObject("RIF_CLASSE_DOC")!=null?rs.getLong("RIF_CLASSE_DOC"):null);
				repFtp.setIpServerFtp(rs.getString("IP_SERVER_FTP"));
				repFtp.setPortServerFtp(rs.getString("PORT_SERVER_FTP"));
				repFtp.setUserNameFtp(rs.getString("USERNAME_FTP"));
				repFtp.setPasswordFtp(rs.getString("PASSWD_FTP"));
			}
			ps.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//log.info("ended method getReportClientiFtpByIdClasseDoc");
		return repFtp;
	}
	
	
	public static ReportPddClasseDocBean getReportPddByIdClasseDoc(Connection cn, Integer idClasseModello) throws SQLException {
		//log.info("started method getReportPddByIdClasseDoc");
		ResultSet rs = null;
		PreparedStatement ps=null;
		ReportPddClasseDocBean repPdd = null;
		
		try {
			String queryString = "SELECT * FROM STORAGE_REPORT_PDD_CLASSSE_DOC WHERE RIF_CLASSE_DOC=? AND ROWNUM=1";
			log.info("Eseguo query in method getReportPddByIdClasseDoc: " + queryString + " - CLASSE_DOC="+idClasseModello);
			ps = cn.prepareStatement(queryString);
			ps.setInt(1, idClasseModello);
			rs = ps.executeQuery();
			while (rs.next()) {		
				repPdd = new ReportPddClasseDocBean();
				repPdd.setIdReportPdd(rs.getObject("ID_REPORT_PDD")!=null?rs.getInt("ID_REPORT_PDD"):null);
				repPdd.setRifClasseDoc(rs.getObject("RIF_CLASSE_DOC")!=null?rs.getInt("RIF_CLASSE_DOC"):null);
				repPdd.setTipoInvioPdd(rs.getObject("TIPO_INVIO_PDD")!=null?rs.getShort("TIPO_INVIO_PDD"):null);
				repPdd.setFlagInvioPdd(rs.getBoolean("FLAG_INVIO_PDD"));
				repPdd.setIpServerFtp(rs.getString("IP_SERVER_FTP"));
				repPdd.setPortServerFtp(rs.getString("PORT_SERVER_FTP"));
				repPdd.setUserNameFtp(rs.getString("USERNAME_FTP"));
				repPdd.setPasswordFtp(rs.getString("PASSWD_FTP"));
				repPdd.setPathPdd(rs.getString("PATH_PDD"));
			}
			ps.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//log.info("ended method getReportPddByIdClasseDoc");
		return repPdd;
	}
	
	
	public static List<MetadatiClasseDocBean> getMetadatiClasseDocByIdClasseDoc(Connection cnSdc, Integer idClasseDoc) throws SQLException {
		//log.info("started method getMetadatiClasseDocByIdClasseDoc");
		ResultSet rs = null;
		PreparedStatement ps=null;
		MetadatiClasseDocBean metadatiBean = null;
		List<MetadatiClasseDocBean> metadatiBeanList = new ArrayList<MetadatiClasseDocBean>();
		try {
			String queryString = "SELECT * FROM TIPOLOG_METADATI_CLASSE_DOC WHERE RIF_CLASSE_DOC = ?";
			log.info("Eseguo query in method getMetadatiClasseDocByIdClasseDoc: " + queryString + " - RIF_CLASSE_DOC: " + idClasseDoc);
			ps = cnSdc.prepareStatement(queryString);
			ps.setInt(1, idClasseDoc);
			rs=ps.executeQuery();
	
			while (rs.next()) {
				metadatiBean = new MetadatiClasseDocBean();
				metadatiBean.setIdMetadato(rs.getObject("ID_METADATO")!=null?rs.getInt("ID_METADATO"):null);
				metadatiBean.setRifClasseDoc(rs.getObject("RIF_CLASSE_DOC")!=null?rs.getInt("RIF_CLASSE_DOC"):null);
				metadatiBean.setLunghezza(rs.getObject("LUNGHEZZA")!=null?rs.getInt("LUNGHEZZA"):null);
				metadatiBean.setTipoDatoMeta(rs.getObject("TIPO_DATO")!=null?rs.getString("TIPO_DATO"):null);
				metadatiBean.setNomeColIndex(rs.getString("NOME_COL_INDEX"));
				metadatiBean.setEtichetta(rs.getString("ETICHETTA"));
				metadatiBean.setSostitutoObbligatorieta(rs.getString("SOSTITUTO_OBBLIGATORIETA"));
				metadatiBean.setFlagDataRiferimento(rs.getBoolean("FLAG_DATA_RIFERIMENTO"));
				metadatiBean.setFlagObbligatorieta(rs.getBoolean("FLAG_OBBLIGATORIETA"));
				metadatiBean.setFlagRicerca(rs.getBoolean("FLAG_RICERCA"));
				metadatiBean.setFlagSequenza(rs.getBoolean("FLAG_SEQUENZA"));
				metadatiBean.setFlagSezionale(rs.getBoolean("FLAG_SEZIONALE"));
				metadatiBean.setFlagUnivocita(rs.getBoolean("FLAG_UNIVOCITA"));
				metadatiBean.setFlagCdc(rs.getBoolean("FLAG_CDC"));	
				metadatiBean.setFlagRepeating(rs.getBoolean("FLAG_REPEATING"));
				metadatiBean.setFlagAgid(rs.getBoolean("FLAG_AGID"));
				metadatiBean.setValoreDefault(rs.getString("VALORE_DEFAULT")!=null?rs.getString("VALORE_DEFAULT"):null);
				metadatiBean.setFlagExternalHash(rs.getBoolean("FLAG_EXTERNAL_HASH"));
				metadatiBeanList.add(metadatiBean);
			}
			ps.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//log.info("ended method getMetadatiClasseDocByIdClasseDoc");
		return metadatiBeanList;
	}


	public static long insertAudit(Connection cn, String user, AuditBean auditBean) throws SQLException {
		log.info("started method insertAudit");
		PreparedStatement ps=null;
		long idAudit = 0;
		
		try {
			idAudit = getSequenceValue(cn, "SEQ_SDC_AUDIT");
			String queryString = "INSERT INTO SDC_AUDIT(ID, IP_SOURCE, USERNAME, AZIONE, CODICE_OGGETTO, TIPO_OGGETTO"
										+ ", DESCRIZIONE, ESITO, DATA_RICHIESTA, APPLICAZIONE) "
								+  "VALUES(?,?,?,?,?,?,?,?,?,?)" ;
			
			log.info("Audit " + auditBean);//id della classe non valorizzato, viene preso da idAudit
			log.info("Eseguo query per insert in Audit: " + queryString);
			
			ps = cn.prepareStatement(queryString);
			ps.setLong(1 , idAudit);
			ps.setString (2 , auditBean.getIpSource());
			ps.setString (3 , auditBean.getUsername());
			ps.setString (4 , auditBean.getAzione());
			ps.setString (5 , auditBean.getCodOggetto());
			ps.setString (6 , auditBean.getTipoOggetto());
			ps.setString (7 , auditBean.getDescrizione());
			ps.setBoolean(8 , auditBean.isEsito());
			ps.setDate(9 , new java.sql.Date(System.currentTimeMillis()) );
			ps.setString (10 , auditBean.getApplicazione());
			int count=ps.executeUpdate();
			ps.close();
			 
			if (count==0) {
				log.error("Errore in insert AUDIT " + auditBean.getDescrizione());
				idAudit=0;
			}

		} catch (SQLException e) {
			idAudit=0;
			e.printStackTrace();
		}
		
		log.info("ended method insertAudit");
		return idAudit;
	}

	
	public static String getProcedura(Connection cn, Integer rifProcedura) throws SQLException  {
		String result=null;
		ResultSet rs = null;
		PreparedStatement ps=null;
	    
		String where = null;
		if (rifProcedura==null) {
			where  = " ID_PROCEDURA IS NULL ";
		} else {
			where  = " ID_PROCEDURA = " + rifProcedura;
		}
		
		try {
			String queryString =  "SELECT TRIM(NOME_PROCEDURA) AS NOME_PROCEDURA "
								+ "FROM TIPOLOG_PROCEDURA "
								+ "WHERE " + where;
			log.info("Eseguo query in method getProcedura: " + queryString+ " - ID_PROCEDURA: " + rifProcedura);
			
			ps = cn.prepareStatement(queryString);
			rs=ps.executeQuery();
			while(rs.next()) {
				result = rs.getString("NOME_PROCEDURA");
			}
			ps.close();
			rs.close();
		
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		//log.info("ended method getProcedura");
		return result;
	}

	
	public static void aggiornaTabParametri(Connection cn, int idClasseModello,String zcode, String dbTabellaParametri, ErroreBean erroreBean) throws SQLException {
		//log.info("started method aggiornaTabParametri");
		PreparedStatement ps;
		int count=0;
		try {
			String queryString = "UPDATE " + dbTabellaParametri+" "
								+"SET FLAG_LAVORATO = ? " 
								+  " ,DESCRIZIONE = ?"
								+  " ,CODICE_ERRORE = ? "
								+  " ,DATA_LAVORAZIONE = ? "
								+"WHERE ID_CLASSE_MODELLO = ? "
								+ " AND ZCODE = ? " ;
			
			//log.info("aggiornaTabParametri(): " + queryString+" - flag_lavorato: "+ erroreBean.getFlagLavoratoNew() +" - descrizione "+ erroreBean.getDescrizioneErrore()
			//							+" - codiceErrore  "+ erroreBean.getCodiceErrore()+ " - idClasseModello" + idClasseModello + " - Zcode "+zcode );
			ps = cn.prepareStatement(queryString);
			ps.setInt(1, erroreBean.getFlagLavoratoNew());
			ps.setString(2, erroreBean.getDescrizioneErrore());
			ps.setString(3, erroreBean.getCodiceErrore());
			ps.setDate(4 , new java.sql.Date(System.currentTimeMillis()) );
			ps.setInt(5, idClasseModello);
			ps.setString(6, zcode);
			count=ps.executeUpdate();
			ps.close();
			
			if (count==0) {
				log.error("Errore in aggiornamento tabella parametri");
			}
			//log.info("ended method aggiornaTabParametri");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public static int insertClasseDocumentale(Connection cn, ClasseDocumentaleBean classeDoc )  {
		PreparedStatement ps = null;
		int result=0,count=0;
		Integer classeDocId=null;
		
		try {
			classeDocId = getSequenceValue(cn, "STORAGE_CLASSEDOC_SEQ");
			log.info("Nuova classe sequence: " + classeDocId);
			
			classeDoc.setLivelloPriorita(1);
			String query = "INSERT INTO STORAGE_CLASSE_DOC ("
						+ " ID_CLASSE_DOC"
						+ ",ID_CLASSE_DOC_OBS"
						+ ",DESCRIZIONE"
						+ ",ZCODE"
						+ ",FLAG_ATTIVA"
						+ ",ESTENSIONE_DOCUMENTI"
						+ ",EXTERNAL_HASH_VERIFICA"
						+ ",NOME_CLASSEDOCUMENTALE"
						+ ",FLAG_EMISSIONE"
						+ ",FLAG_ARCHIVIAZIONE"
						+ ",FLAG_CONSERVAZIONE"
						+ ",RIF_TIPOLOGIA"
						+ ",PATH_IN"
						+ ",PATH_BACKUP_PDV"
						+ ",RIF_CLASS_DATO"
						+ ",RIF_FIRMATARIO"
						+ ",RIF_PERIODO_RETENTION"
						+ ",CTRL_UNIVOCITA"
						+ ",CTRL_SEQUENZIALITA"
						+ ",FLAG_SEZIONALE"
						+ ",FLAG_CDC"
						+ ",PATH_ARCHIVIAZIONE"
						+ ",FLAG_CIFRATO"
						+ ",LIVELLO_PRIORITA"
						+ ",RIF_WORKFLOW"
						+ ",RIF_CODICE_ACCOUNTING"
						+ ",RIF_TERMINE_CONSERVAZIONE"
						+ ",RIF_PROCEDURA"
						+ ",RIF_FIRMATARIO_RESP_CONS"  
						+" ,TIPO_CLASSE_DOC"
						+ ",GIORNI_BACKUP_PDV"
						+ ",FLAG_DATA_CERTA"
						+ ",FLAG_TIMESTAMP"
						+ ",FLAG_TRACKING_GED"
						+ ",MESE_RIFERIMENTO"
						+ ",FLAG_BOLLATO"
						+ ",PATH_PRESCARTO"
						+ ",PATH_SCARTO"
						+ ",RIF_TIPOLOGIA_FIRMA"
						+ ",FLAG_SMISTAMENTO"
						+ ",FLAG_SDCID"
						+ ",FLAG_CONTROLLA_FIRMA_PDF"
						+ ",FLAG_NOTIFICA_ORCHESTRATORE"
						+ ",RIF_MODELLO_AGID"

						+ ",FLAG_DISMESSA"//non accetta null, inserita qui rispetto a SdCWeb
						
						+ ")" +
						// campi nullable PATH_EMISSIONE,RIFERIMENTO,GIORNI_CHIUSURA_PDA,RIF_TIPO_VALIDAZIONE,FLAG_DISMESSA,RIF_TERMINE_CONSERVAZIONE,CTRL_DEROGA,RIF_PROCEDURA,MAX_DOCUMENTI_PDA,MAX_MB_PDA,RIF_FIRMATARIO_RESP_CONS
						"VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			
			log.info("start query insertClasseDocumentale "+ classeDoc.getNomeClasseDocumentale() + " - " + query);
	
			ps = cn.prepareStatement(query);
			ps.setInt(1, classeDocId);
			ps.setInt(2, 0);
			ps.setString(3, classeDoc.getDescrizione());
			ps.setString(4, classeDoc.getzCode());
			ps.setBoolean(5, classeDoc.isFlagAttiva());
			ps.setString(6, classeDoc.getEstensioneDocumenti());
			ps.setBoolean(7, classeDoc.isExternalHashVerifica());
			ps.setString(8, classeDoc.getNomeClasseDocumentale());
			ps.setBoolean(9, classeDoc.isFlagEmissione());
			ps.setBoolean(10, classeDoc.isFlagArchiviazione());
			ps.setBoolean(11, classeDoc.isFlagConservazione());
			ps.setInt(12, classeDoc.getRifTipologia());
			ps.setString(13, classeDoc.getPathIn());
			ps.setString(14, classeDoc.getPathBackupPdv());
			ps.setInt(15, classeDoc.getRifClassDato());

			ps.setObject(16, classeDoc.getRifFirmatario());//object invece di int
			
			ps.setInt(17, classeDoc.getRifPeriodoRetention());
			ps.setBoolean(18, classeDoc.isCtrlUnivocita());
			ps.setBoolean(19, classeDoc.isCtrlSequenzialita());
			ps.setBoolean(20, classeDoc.isFlagSezionale());
			ps.setBoolean(21, classeDoc.isFlagCdc());
			ps.setString(22, classeDoc.getPathArchiviazione());
			ps.setBoolean(23, classeDoc.isFlagCifrato());
			ps.setInt(24, classeDoc.getLivelloPriorita());
			ps.setInt(25, classeDoc.getRifWorkflow());
			ps.setInt(26, classeDoc.getRifCodiceAccounting());
			ps.setInt(27, classeDoc.getRifTermineConservazione());
			ps.setInt(28, classeDoc.getRifProcedura());
			ps.setInt(29, classeDoc.getRifFirmatarioRespCons());
			ps.setString(30, classeDoc.getTipoClasseDoc());
			
			ps.setObject(31, classeDoc.getGiorniBackupPdv() );//object invece di int
			
			ps.setBoolean(32, classeDoc.isFlagDataCerta());
			ps.setBoolean(33, classeDoc.isFlagTimestamp());
			ps.setBoolean(34, classeDoc.isFlagTrackingGed());
			ps.setInt(35, classeDoc.getMeseRiferimento());
			ps.setBoolean(36, classeDoc.isFlagBollato());
			ps.setString(37, classeDoc.getPathPrescarto());
			ps.setString(38, classeDoc.getPathScarto());
			ps.setObject(39, classeDoc.getRifTipologiaFirma());//object invece di int
			ps.setBoolean(40, classeDoc.isFlagSmistamento());
			ps.setBoolean(41, classeDoc.isFlagSdcId());
			ps.setBoolean(42, classeDoc.isFlagControllaFirmaPdf());
			ps.setBoolean(43, classeDoc.isFlagNotificaOrchestratore());
			ps.setObject(44, classeDoc.getRifModelloAgid());//object invece di int			
	
			//aggiunto rispetto a SdCWeb
			ps.setBoolean(45, classeDoc.isFlagDismessa());			
			count=ps.executeUpdate();
			ps.close();

			//log.info("end query insertClasseDocumentale"+ classeDoc.getNomeClasseDocumentale());
			if (count==0) {
				classeDocId=null;
				log.error("Errore in INSERT in STORAGE_CLASSE_DOC " + classeDocId);
			} 
			
		} catch(Exception e){
			classeDocId=null;	  
			e.printStackTrace();
		}
		
		return classeDocId;
	}


	public static String getTablespaceName(Connection cn, String tableName) throws SQLException {
		//log.info("started method getTablespaceName");
		PreparedStatement ps = null;
		ResultSet rs = null;
		String estDoc=null;
		int count=0;
		
		try {
			String query = "SELECT TABLESPACE_NAME FROM user_tab_partitions WHERE table_name=? AND ROWNUM=1";
			//log.info("Eseguo query: " + query + " - table_name " + tableName + " - table_name: " +  tableName);
			
			//String result = this.getJdbcTemplate().queryForObject(query, new RowMapper<String>() {
			//	public String mapRow(ResultSet rs, int rowNum) throws SQLException {
			//		String estDoc = rs.getString("TABLESPACE_NAME");
			//		return estDoc;
			//		} 
			//	}, tableName);
			
			ps = cn.prepareStatement(query);
			ps.setString(1,tableName);
			rs=ps.executeQuery();
			while (rs.next()) {
				estDoc = rs.getString("TABLESPACE_NAME");
			}
			ps.close();
			rs.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//log.info("ended method getTablespaceName. Result:"+ estDoc);
		return estDoc;
	}
	
	
	public static boolean checkIfPartitionExistsOnTable(Connection cn, String partitionName, String tableName) throws SQLException {
		//log.info("started method checkIfPartitionExistsOnTable");
		Integer result=null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String query = "SELECT COUNT(*) FROM ALL_TAB_PARTITIONS WHERE UPPER(PARTITION_NAME)=UPPER(?) AND TABLE_NAME=?";
			//log.info("Eseguo query in method checkIfPartitionExistsOnTable: " + query + " - PARTITION_NAME " + partitionName + " - TABLE_NAME = "+ tableName);

			ps = cn.prepareStatement(query);
			ps.setString(1,partitionName);
			ps.setString(2,tableName);
			rs=ps.executeQuery();
			while (rs.next()) {
				result = rs.getObject(1)!=null?rs.getInt(1):null;
			}
			ps.close();
			rs.close();
			
		} catch (SQLException e) {
			//result=null;
			e.printStackTrace();
		}
		//log.info("checkIfPartitionExistsOnTable: " + result);
		return result!=null && result>0;
		//return result;
		
	}
	
	
	public static Integer createPartition(Connection cn, String tableName, String partitionName, Integer partitionId, String tablespaceName) {	
			log.info("started method createPartition");	
			Integer result = null;
			PreparedStatement ps = null;
			int count=0;
	
			try {
				String query = "ALTER TABLE " + tableName + " SPLIT PARTITION ALTRI VALUES ( " + partitionId + " ) "
								+ "INTO ( PARTITION " + partitionName + " TABLESPACE " + tablespaceName + ", "
								+ "PARTITION ALTRI TABLESPACE " + tablespaceName +  " )";	
		
				log.info("start query createPartition "+ query);	
				//getJdbcTemplate().execute(query);
				ps = cn.prepareStatement(query);
				count=ps.executeUpdate();
				ps.close();
				
				if (count==0) {
					log.error("Partizione NON creata: partitionId: "+partitionId+", partitionName: " + partitionName + ", tablespace: " + tablespaceName);
					result = null;
				} else {
					log.info("Partizione creata: partitionId: "+partitionId+", partitionName: " + partitionName + ", tablespace: " + tablespaceName);
					result = 1;
				}
			
		} catch (SQLException e) {
			result = null;
			e.printStackTrace();
		}
		return result;	
	}	

	
	public static Integer insertReportClientiClasseDoc(Connection cn, ClasseDocumentaleBean classeDoc) throws SQLException {
		PreparedStatement ps=null;
		Integer repId = null;
		int count=0;
		try {
			repId = getSequenceValue(cn,"SEQ_REPORT_CLIENTI");
			String query = "INSERT INTO STORAGE_REPORT_CLIENTI_CLASSE_DOC(ID_REPORT_CLIENTI,RIF_CLASSE_DOC,INDICI_RAPPORTO_CONSERVAZIONE,TIPO_IDENTIFICATIVO_ESTERNO,FLAG_INVIO_RDV,PATH_RAPPORTO_VERSAMENTO,TIPO_INVIO_RDV,FLAG_INVIO_RDA,PATH_RAPPORTO_ARCHIVIAZIONE,TIPO_INVIO_RDA)" + 
					" VALUES(?,?,?,?,?,?,?,?,?,?)";
			log.info("start query insertReportClientiClasseDoc - classe di riferimento: ("+ classeDoc.getReportClasseDoc().getRifClasseDoc() + ") - " + query);

			//getJdbcTemplate().update(query, repId,reportClasseDoc.getRifClasseDoc(),reportClasseDoc.getIndiciRapportoConservazione(),reportClasseDoc.getTipoIdentificativoEsterno(),reportClasseDoc.getFlagInvioRdv(),reportClasseDoc.getPathRapportoVersamento(),reportClasseDoc.getTipoInvioRdv(),reportClasseDoc.getFlagInvioRda(),reportClasseDoc.getPathRapportoArchiviazione(), reportClasseDoc.getTipoInvioRda());
			ps = cn.prepareStatement(query);
			ps.setInt (1,    repId);
			ps.setLong (2,   classeDoc.getIdClasseDoc());
			ps.setShort (3,  classeDoc.getReportClasseDoc().getIndiciRapportoConservazione());
			ps.setShort (4,  classeDoc.getReportClasseDoc().getTipoIdentificativoEsterno());
			ps.setShort (5,  classeDoc.getReportClasseDoc().getFlagInvioRdv());
			ps.setString (6, classeDoc.getReportClasseDoc().getPathRapportoVersamento());
			ps.setShort (7,  classeDoc.getReportClasseDoc().getTipoInvioRdv());
			ps.setShort (8,  classeDoc.getReportClasseDoc().getFlagInvioRda());
			ps.setString (9, classeDoc.getReportClasseDoc().getPathRapportoArchiviazione());
			ps.setShort (10, classeDoc.getReportClasseDoc().getTipoInvioRda());
			count=ps.executeUpdate();
			ps.close();
			
			if (count==0) {
				repId = null;
				log.error("Errore in INSERT into STORAGE_REPORT_CLIENTI_CLASSE_DOC");
			} 
		} catch (SQLException e) {
			repId = null;
			e.printStackTrace();
		}
		return repId;
	}


	
	public static Integer insertReportFtpClasseDoc(Connection cn, ClasseDocumentaleBean classeDoc) throws SQLException {
		PreparedStatement ps=null;
		Integer repId=null;
		
		try {
			repId = getSequenceValue(cn,"SEQ_REPORT_CLIENTI_FTP");
			String query = "INSERT INTO STORAGE_REPORT_CLIENTI_FTP(ID_REPORT_CLIENTI_FTP,RIF_CLASSE_DOC,IP_SERVER_FTP,PORT_SERVER_FTP,USERNAME_FTP,PASSWD_FTP)" + 
					" VALUES(?,?,?,?,?,?)";
			log.info("start query insertReportFtpClasseDoc - classe di riferimento (" + classeDoc.getReportFtp().getRifClasseDoc() + ")" );
			
			ps = cn.prepareStatement(query);
			ps.setInt(1,    repId);
			ps.setLong(2,   classeDoc.getIdClasseDoc()); 
			ps.setString(3, classeDoc.getReportFtp().getIpServerFtp());
			ps.setString(4, classeDoc.getReportFtp().getPortServerFtp());
			ps.setString(5, classeDoc.getReportFtp().getUserNameFtp());
			ps.setString(6, classeDoc.getReportFtp().getPasswordFtp());				
			int count=0;
			
			count=ps.executeUpdate();
			ps.close();
			//getJdbcTemplate().update(query, repId,reportFtp.getRifClasseDoc(),reportFtp.getIpServerFtp(), reportFtp.getPortServerFtp(), reportFtp.getUserNameFtp(), reportFtp.getPasswordFtp());
			
			if (count==0) {
				repId=null;
				log.error("Errore in INSERT into STORAGE_REPORT_CLIENTI_FTP");
			}
		} catch (SQLException e) {
			repId=null;
			e.printStackTrace();
		}
		
		return repId;
	}
	

	public static Integer insertReportPddClasseDoc(Connection cn, ClasseDocumentaleBean classeDoc)  {
		PreparedStatement ps=null;
		Integer repId = null;
		int count=0;

		try {
			repId = getSequenceValue(cn, "STORAGE_REPORT_PDD_SEQ");
			String query = "INSERT INTO STORAGE_REPORT_PDD_CLASSSE_DOC(ID_REPORT_PDD,RIF_CLASSE_DOC,PATH_PDD,TIPO_INVIO_PDD,IP_SERVER_FTP,PORT_SERVER_FTP,USERNAME_FTP,PASSWD_FTP,FLAG_INVIO_PDD)" + 
					" VALUES(?,?,?,?,?,?,?,?,?)";
			
			log.info("start query in method insertReporPddClasseDoc - classe di riferimento (" + classeDoc.getReportPdd().getRifClasseDoc()+") - "+  query );
			//getJdbcTemplate().update(query, repId,reportPdd.getRifClasseDoc(),reportPdd.getPathPdd(), reportPdd.getTipoInvioPdd(),reportPdd.getIpServerFtp(), reportPdd.getPortServerFtp(), reportPdd.getUserNameFtp(), reportPdd.getPasswordFtp(), reportPdd.getFlagInvioPdd());
			
			ps = cn.prepareStatement(query);
			ps.setInt(1, repId);
			ps.setLong(2, classeDoc.getIdClasseDoc());
			ps.setString(3,  classeDoc.getReportPdd().getPathPdd());
			ps.setShort(4,   classeDoc.getReportPdd().getTipoInvioPdd());
			ps.setString(5,  classeDoc.getReportPdd().getIpServerFtp());
			ps.setString(6,  classeDoc.getReportPdd().getPortServerFtp());
			ps.setString(7,  classeDoc.getReportPdd().getUserNameFtp());
			ps.setString(8,  classeDoc.getReportPdd().getPasswordFtp());
			ps.setBoolean(9, classeDoc.getReportPdd().getFlagInvioPdd());

			count=ps.executeUpdate();
			ps.close();
			
			//count=0;
			
			if (count==0) {
				log.error("Errore in INSERT into STORAGE_REPORT_PDD_CLASSSE_DOC");
				repId=null;
			}
	
		} catch (SQLException e) {
			repId=null;
			e.printStackTrace();
		}
		return repId;
	}
	
		
	public static Integer insertMetadatoClasseDoc(Connection cn, MetadatiClasseDocBean metaBean, int rifClasseDoc) {
		//log.info("stated method insertMetadatoClasseDoc ");
		Integer metaId =null;
		int count=0;

		try {
			PreparedStatement ps = null;
			metaId = getSequenceValue(cn,"TIPOLOG_METADATI_CLASSEDOC_SEQ");

			String query = "INSERT INTO TIPOLOG_METADATI_CLASSE_DOC(ID_METADATO,RIF_CLASSE_DOC,ETICHETTA,FLAG_CDC,FLAG_RICERCA,FLAG_SEZIONALE,FLAG_UNIVOCITA,LUNGHEZZA," + 
					"TIPO_DATO,NOME_COL_INDEX,FLAG_OBBLIGATORIETA,SOSTITUTO_OBBLIGATORIETA,FLAG_SEQUENZA,FLAG_DATA_RIFERIMENTO,FLAG_REPEATING," + 
					"VALORE_DEFAULT,FLAG_AGID,RIF_MODELLO_AGID,FLAG_EXTERNAL_HASH)" + 
					" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			
			//log.info("start query insertMetadatoClasseDoc rifClasseDoc " + ((MetadatiClasseDocBean) metaBean).getRifClasseDoc() + " " + query);
			
			//getJdbcTemplate().update(query, metaId,metaBean.getRifClasseDoc(),metaBean.getEtichetta(), metaBean.isFlagCdc(),metaBean.isFlagRicerca(), metaBean.isFlagSezionale(), metaBean.isFlagUnivocita(), metaBean.getLunghezza(),
			//		metaBean.getTipoDatoMeta(), metaBean.getNomeColIndex(), metaBean.isFlagObbligatorieta(), metaBean.getSostitutoObbligatorieta(), metaBean.isFlagSequenza(), metaBean.isFlagDataRiferimento(), metaBean.isFlagRepeating(),
			//		metaBean.getValoreDefault(), metaBean.isFlagAgid(), metaBean.getRifModelloAgid(), metaBean.isFlagExternalHash());
			
			ps = cn.prepareStatement(query);
			ps.setInt(1 ,     metaId);
			ps.setInt(2 ,     metaBean.getRifClasseDoc());
			ps.setString(3 ,  metaBean.getEtichetta());
			ps.setBoolean(4 , metaBean.isFlagCdc());
			ps.setBoolean(5 , metaBean.isFlagRicerca());
			ps.setBoolean(6 , metaBean.isFlagSezionale());
			ps.setBoolean(7 , metaBean.isFlagUnivocita());
			ps.setInt(8 ,     metaBean.getLunghezza());
			ps.setString(9 ,  metaBean.getTipoDatoMeta());
			ps.setString(10 , metaBean.getNomeColIndex());
			ps.setBoolean(11 ,metaBean.isFlagObbligatorieta());
			ps.setString(12 , metaBean.getSostitutoObbligatorieta());
			ps.setBoolean(13 ,metaBean.isFlagSequenza());
			ps.setBoolean(14 ,metaBean.isFlagDataRiferimento());
			ps.setBoolean(15 ,metaBean.isFlagRepeating());
			ps.setString(16 , metaBean.getValoreDefault());
			ps.setBoolean(17, metaBean.isFlagAgid());
			ps.setObject(18,  metaBean.getRifModelloAgid());
			ps.setBoolean(19, metaBean.isFlagExternalHash());

			count=ps.executeUpdate();
			ps.close();
			if (count==0) {
				log.error("Errore in INSERT into TIPOLOG_METADATI_CLASSE_DOC");
				metaId=null;
			}
			
		} catch (SQLException e) {
			metaId=null;
			e.printStackTrace();
		}
		//log.info("ended method insertMetadatoClasseDoc");
		return metaId;
	}
	
	
	public static List<String> getListUsernameAmministratori(Connection cn) throws SQLException {
		//log.info("started method getListUsernameAmministratori");
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<String> results = null;
		
		try {
			String query = "SELECT LU.NOME FROM SDC_USR LU,SDC_PROFILE LP WHERE LP.ID_PROFILE = LU.ID_PROFILE_FK AND LOWER(LP.NAME) = LOWER('sdc_admin')";
			log.info("Eseguo query: " + query);
			ps = cn.prepareStatement(query);
			rs=ps.executeQuery();
			results = new ArrayList<String>();
			while (rs.next()) {
				results.add(rs.getString("NOME"));
			}
			ps.close();
			rs.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//log.info("ended method getListUsernameAmministratori");
		return results;
	}	

	
	public static Integer insertClasseDocAssociation(Connection cn, String username, Integer selectedClassedoc, String loggedUser) throws SQLException {
		//log.info("started method insertClasseDocAssociation");
		PreparedStatement ps = null;
		int count=0;
		Integer result=null;
		
		try {
			//commentato anche in SdCWeb --> String query = "INSERT INTO SDC_USR_CLASSE_DOC (USERNAME,RIF_CLASSE_DOC) VALUES (?,?)";
			String query = "MERGE INTO SDC_USR_CLASSE_DOC USING DUAL ON (USERNAME=? AND RIF_CLASSE_DOC=?) WHEN NOT MATCHED THEN INSERT (USERNAME,RIF_CLASSE_DOC) VALUES (?,?)";
			//log.info("query insertClasseDocAssociation rifClasseDoc "+ selectedClassedoc + " " + query);
				
			ps = cn.prepareStatement(query);
			ps.setString(1, username);
			ps.setInt(2, selectedClassedoc);
			ps.setString(3, username);
			ps.setInt(4, selectedClassedoc);
	
			count=ps.executeUpdate();
			ps.close();
			if (count==0) {
				log.error("Errore in INSERT into TIPOLOG_METADATI_CLASSE_DOC");
				result = null;
			}
			result=1;
			//return getJdbcTemplate().update(query, username, selectedClassedoc,  username, selectedClassedoc);

		} catch (SQLException e) {
			result = null;
			e.printStackTrace();
		}

		//log.info("ended method insertClasseDocAssociation");
		return result;
	}
	
	
	private static Integer getSequenceValue(Connection cn, String sequenceName) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int sequenceValue = 0;
		try {
			String queryString = "SELECT "+sequenceName+".NEXTVAL SEQ_VALUE FROM DUAL";
			ps = cn.prepareStatement(queryString);
			rs=ps.executeQuery();
			while (rs.next()) {
				sequenceValue = rs.getObject("SEQ_VALUE")!=null?rs.getInt("SEQ_VALUE"):null; 
			}
			ps.close();
			rs.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sequenceValue;
	}
	
	
	/*
	public static int getLastIdSdc(Properties appProperties) throws SQLException, ClassNotFoundException {
		//log.info("Method getLastIdSdc started");
		Connection connId = null;
		ResultSet rsId = null;
		PreparedStatement psId=null;

		String dbDriver = (String) appProperties.get("db.driver");
		String dbUrlDest = (String) appProperties.get("db.url.dest");
		String dbUsernameDest = (String) appProperties.get("db.username.dest");
		String dbPasswordDest = (String) appProperties.get("db.password.dest");
		int lastId=0;
		String query="";
		try {
			Class.forName(dbDriver);
			connId = DriverManager.getConnection(dbUrlDest,dbUsernameDest,dbPasswordDest);
			query = "SELECT TO_NUMBER(MAX(ID_DCTM_ARCHIVIAZIONE)) as maxId FROM TIPOLOG_DCTM_ARCHIVIAZIONE";
			psId = connId.prepareStatement(query);
	        rsId = psId.executeQuery();

	        if (rsId.next()) {
	        	lastId=rsId.getInt("maxId");			
	        }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeConnection(connId,rsId,psId);
		}
		//log.info("Method getLastIdSdc ended");
		return lastId;
	}	
	*/
	
	/*
	public static boolean getStorageClasseDoc(Properties appProperties, int classe) throws SQLException, ClassNotFoundException {
		//log.info("Method getStorageClasseDoc started");
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps=null;

		String dbDriver = (String) appProperties.get("db.driver");
		String dbUrlDest = (String) appProperties.get("db.url.dest");
		String dbUsernameDest = (String) appProperties.get("db.username.dest");
		String dbPasswordDest = (String) appProperties.get("db.password.dest");
		int found=0;
		boolean flagFound=false; 
		String query="";
		try {
			Class.forName(dbDriver);
			conn = DriverManager.getConnection(dbUrlDest,dbUsernameDest,dbPasswordDest);
			query = "SELECT 1 as found FROM STORAGE_CLASSE_DOC WHERE ROWNUM = 1 AND ID_CLASSE_DOC = ? ";
			log.info("Eseguo query: " + query+ "(Classe="+classe+")");
			ps = conn.prepareStatement(query);
			ps.setInt(1, classe);
	        rs = ps.executeQuery();

	        if (rs.next()) {
	        	found=rs.getInt("found");
	        	flagFound=true;
	        }
	        

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeConnection(conn,rs,ps);
		}
		//log.info("Method getStorageClasseDoc ended");
		return flagFound;
	}	
	*/
	
	/*
	public static List<CodBean> getDatiCod(Properties appProperties) throws Exception {
		log.info("method getDatiCod Started");
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps=null;
		
		String dbDriver = (String) appProperties.get("db.driver");
		String dbUrlOri = (String) appProperties.get("db.url.ori");
		String dbUsernameOri = (String) appProperties.get("db.username.ori");
		String dbPasswordOri = (String) appProperties.get("db.password.ori");
		
		List<CodBean> result = new ArrayList<CodBean>();
		
		try{
			Class.forName(dbDriver);
			conn = DriverManager.getConnection(dbUrlOri,dbUsernameOri,dbPasswordOri);
		 	
			// per cod collaudo, 
			//	 cambiare lav.lavorazioneid -> lav.sdclavorazioneid
			//	 cambiare PR_CODG_QUERYLAYER.aoslavorazioni -> TST_CODG_QUERYLAYER.aoslavorazioni lav,
			
		 	String query = " select lav.lavorazioneid as lavorazioneId, \n"
						   +"       cfg_repo.nome_repositorio as nomeRepo, \n"
						   +"       cfg_data.mida_code_field_code_name as midaCodeField, \n"
						   +"       lav.campo_numeropagine as numPagine, \n"
						   +"       SUBSTR(cfg_data.extraction_query_cond,0,INSTR(cfg_data.extraction_query_cond,'=')-1) as campoStatoSostitut,\n"
						   +"       SUBSTR(cfg_data.extraction_query_cond,INSTR(cfg_data.extraction_query_cond,'=')+1) as statoSostitut,\n"
						   +"       '2' as statoSostitutInviatoCons, \n"
						   +"       '1' as statoSostitutErroreCons,  \n"
						   +"       '10' as maxCpx, \n"
						   +"       cfg_repo.username as repoUsername, \n"
						   +"       cfg_repo.password as repoPwd,\n"
						   +"       cfg_cs.file_dfc_properties as dfcProp, \n"
						   +"       classi.xml_class_doc_map as classiDocMap,\n"
						   +"       classi.nome_classedocumentale_dctm as nomeClasseDctm \n" 
						   +" from  PR_CODG_QUERYLAYER.aoslavorazioni lav, \n"
						   +"       classi_documentali classi, \n"
						   +"       CFG_DATA_EXPORTER cfg_data, \n"
						   +"       CFG_REPOSITORIO_DOCUMENTUM cfg_repo, \n"
						   +"       CFG_CONTENT_SERVER cfg_cs \n"
						   +" where lav.idclassedoc = classi.idclassedocumentale and \n"
						   +"       classi.id_exporter_instance = cfg_data.id_exporter_instance and \n"
						   +"       cfg_repo.id_repositorio = classi.id_repositorio and \n"
						   +"       cfg_repo.id_content_server = cfg_cs.id_content_server and \n"
						   +"       lav.lavorazioneid is not null \n"
						   +" order by lav.lavorazioneid";			


		 	//----------------------------------------------------------------
			//test db locale sys
			//query = " select LavorazioneId as lavorazioneId,nomeRepo,midaCodeField,numPagine,campoStatoSostitut,statoSostitut,\n"
			//		   +"    statoSostitutInviatoCons,statoSostitutErroreCons,maxCpx,repoUsername,repoPwd,dfcProp,classiDocMap,nomeClasseDctm \n" 
			//		   +" from  TEO_TEST_2 "
			//		   + " where rownum between 1 and 3";
			//----------------------------------------------------------------		 	
		 	
		 	log.info("Eseguo query: " + query);

			ps = conn.prepareStatement(query);
	        rs = ps.executeQuery();		

	        log.info("Carico le classi...");
			while(rs.next()) {
				CodBean obj = new CodBean();
				//obj.setNewId(newId); //ID_DCTM_ARCHIVIAZIONE 
				obj.setLavorazioneId(rs.getInt("lavorazioneId")); //RIF_CLASSE_DOC 
		        obj.setNomeRepo(rs.getString("nomeRepo"));    //REPOSITORY 
				obj.setMidaCodeField(rs.getString("midaCodeField"));//CAMPO_CODICE_MIDA 
				obj.setNumPagine(rs.getString("numPagine"));//CAMPO_NUMERO_PAGINE 
				obj.setCampoStatoSostitut(rs.getString("campoStatoSostitut"));//CAMPO_STATO_SOSTITUTIVA 
				obj.setStatoSostitut(rs.getString("statoSostitut"));//STATO_SOSTITUTIVA_PRONTO_CONS 
				obj.setStatoSostitutInviatoCons(rs.getString("statoSostitutInviatoCons"));//STATO_SOSTITUTIVA_INVIATO_CONS 
				obj.setStatoSostitutErroreCons(rs.getString("statoSostitutErroreCons"));//STATO_SOSTITUTIVA_ERRORE_CONS 
				obj.setMaxCpx(rs.getString("maxCpx"));//MAX_CPX 
				obj.setRepoUsername(rs.getString("repoUsername"));//DCTM_USERNAME 
				obj.setRepoPwd(rs.getString("repoPwd"));//DCTM_PASSWORD 
				obj.setDfcProp(rs.getString("dfcProp"));//DFC 
				obj.setClassiDocMap(rs.getString("classiDocMap"));//METADATI
				obj.setNomeClasseDctm(rs.getString("nomeClasseDctm"));//NOME_CLASSE_DOC 	      
				result.add(obj);
				
				//log.info("salvo classe in result COD: "+rs.getInt("lavorazioneId"));
			}
		
		} catch(SQLException e){
			log.error("Dettaglio Errore getDatiCod: "+ e);
			throw new Exception(e);
		
		} finally{
			closeConnection(conn,rs,ps);
		}
		log.info("method getDatiCod Ended");
		return result;
	}
	*/
	/*
	public static int InsertIntoSdc(Properties appProperties,List<CodBean> datiCod ) throws SQLException, ClassNotFoundException {
		//log.info("Method InsertIntoSdc Started" );
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps=null;
		
		String dbDriver = (String) appProperties.get("db.driver");
		String dbUrlDest = (String) appProperties.get("db.url.dest");
		String dbUsernameDest = (String) appProperties.get("db.username.dest");
		String dbPasswordDest = (String) appProperties.get("db.password.dest");
		
		String query="";
		String xmlForUnMarshalling=null;
		String xmlMarshalling = null;
		int newId=0,oldId=0,totWrite=0,totWrite2=0;
        
		try {
			Class.forName(dbDriver);
			conn = DriverManager.getConnection(dbUrlDest,dbUsernameDest,dbPasswordDest);
			
			//disattiva commit automatico
			conn.setAutoCommit(false);
			query =   " Insert into TIPOLOG_DCTM_ARCHIVIAZIONE ("
					+ "             ID_DCTM_ARCHIVIAZIONE,"
					+ "				RIF_CLASSE_DOC,"
					+ "				REPOSITORY,"
					+ "				CAMPO_CODICE_MIDA,"
					+ "				CAMPO_NUMERO_PAGINE,"
					+ "				CAMPO_STATO_SOSTITUTIVA,"
					+ "				STATO_SOSTITUTIVA_PRONTO_CONS,"
					+ "				STATO_SOSTITUTIVA_INVIATO_CONS,"
					+ "				STATO_SOSTITUTIVA_ERRORE_CONS,"
					+ "				MAX_CPX,"
					+ "				DCTM_USERNAME,"
					+ "				DCTM_PASSWORD,"
					+ "				DFC,"
					+ "				METADATI,"
					+ "				NOME_CLASSE_DOC) "
					+ " values "
					+ " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			ps = conn.prepareStatement(query);

			for(int i=0; i<datiCod.size(); i++){
				int lavorazioneId=0;
				lavorazioneId=datiCod.get(i).getLavorazioneId();
				log.info("");
				log.info("Classe " + lavorazioneId+" da inserire");
				
				
				log.info("Verifica assenza classe "+lavorazioneId+"(column: RIF_CLASSE_DOC) in tabella di destinazione ");
				boolean flagAssenzaClasse=false;
				flagAssenzaClasse=VerificaAssenzaClasse( lavorazioneId, appProperties );
				if (flagAssenzaClasse) {
					log.info("Verifica assenza OK"); //se true=ok
				} else {
					log.info("Classe già presente in tabella di destinazione, nessuna insert");
					continue;
				}
				
				
				log.info("Verifica presenza classe "+lavorazioneId+" in tabella STORAGE_CLASSE_DOC");
				boolean flagPresenzaClasseStorage=false;
				flagPresenzaClasseStorage=getStorageClasseDoc(appProperties,lavorazioneId );
				if (flagPresenzaClasseStorage) {
					log.info("Verifica presenza OK");
				} else {
					log.info("Classe non presente in tabella STORAGE_CLASSE_DOC, nessuna insert");
					continue;
				}
				

				//Mette sotto IF l'inserimento del rec se 
				//la classe non esiste in TIPOLOG_DCTM_ARCHIVIAZIONE ed esiste in STORAGE_CLASSE_DOC
				if ( (flagAssenzaClasse==false) || (flagPresenzaClasseStorage==false) ) {
					log.error("Classe gia' presente in TIPOLOG_DCTM_ARCHIVIAZIONE oppure NON presente in STORAGE_CLASSE_DOC: " + lavorazioneId);
				} else {
					if ( (flagAssenzaClasse==true) && (flagPresenzaClasseStorage==true)) { 
						log.info("Eseguo insert per verifiche ok");
						
						
						log.info("Recupera last id in tabella destinazione e lo incremento di 1");
						newId=getLastIdSdc(appProperties);
						oldId=newId;
						newId=newId+1;
						log.info("Last_id "+oldId+", NewId: " + newId);
						
						
						//JAXB Unmarshalling dei campi/stringa campo xml di origine (classi.xml_class_doc_map as classiDocMap)
						log.info("Eseguo unMarshalling per stringa xml di origine");
						xmlForUnMarshalling = datiCod.get(i).getClassiDocMap(); 
						List<String> objectMapList = null; 
						objectMapList=EseguiUnmarshalling(xmlForUnMarshalling,appProperties);
						log.info("Valori per attributi xml: " + objectMapList.toString());
						log.info("unMarshalling: OK");
						
						
						//JAXB Marshalling 
						log.info("Eseguo Marshalling per campo METADATI di destinazione");
						try {
							xmlMarshalling = EseguiMarshalling(objectMapList);
						} catch (JAXBException e) {
							e.printStackTrace();
						}
						log.info("Marshalling: OK");
						
						//crea xml (non utilizzato)
						//xmlOutString = CreaXml(objectMapList, appProperties);
						//log.info("xml di destinazione:\n" + xmlOutString);
	
						log.info("Insert nuovo ID, Classe e Repo: " + newId+","+datiCod.get(i).getLavorazioneId()+","+datiCod.get(i).getNomeRepo());
						ps.setInt(1, newId);
					    ps.setInt(2, datiCod.get(i).getLavorazioneId());
					    ps.setString(3, datiCod.get(i).getNomeRepo());
					    ps.setString(4, datiCod.get(i).getMidaCodeField());
					    ps.setString(5, datiCod.get(i).getNumPagine());
					    ps.setString(6, datiCod.get(i).getCampoStatoSostitut());
					    ps.setString(7, datiCod.get(i).getStatoSostitut());
					    ps.setString(8, datiCod.get(i).getStatoSostitutInviatoCons());
					    ps.setString(9, datiCod.get(i).getStatoSostitutErroreCons());
					    ps.setString(10, datiCod.get(i).getMaxCpx());
					    ps.setString(11, datiCod.get(i).getRepoUsername());
					    ps.setString(12, datiCod.get(i).getRepoPwd());
					    ps.setString(13, datiCod.get(i).getDfcProp());
					    
					    //ps.setString(14, xmlOutString); //da metodo creaXml
					    ps.setString(14, xmlMarshalling); //da JAXB
					    ps.setString(15, datiCod.get(i).getNomeClasseDctm());
	
					    ps.addBatch();
					}
					ps.getResultSet();
					
					int[] rows = ps.executeBatch();
					totWrite=totWrite+rows.length;
	
					conn.commit();		
				}//end if
			}//end for			
					
			
		} catch (SQLException e) {
			System.err.println("SQLException information");
			totWrite--;
			e.printStackTrace();
		} finally {
			closeConnection(conn,rs,ps);
		}
		
		//log.info("Method InsertIntoSdc Ended");
		
	return totWrite;
	}
	*/
	
	
	/*
	private static boolean VerificaAssenzaClasse(int lavorazioneId, Properties appProperties) throws SQLException, ClassNotFoundException {
		//log.info("method VerificaAssenzaClasse started");
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		String dbDriver = (String) appProperties.get("db.driver");
		String dbUrlDest = (String) appProperties.get("db.url.dest");
		String dbUsernameDest = (String) appProperties.get("db.username.dest");
		String dbPasswordDest = (String) appProperties.get("db.password.dest");

		String query="";
		boolean flagAssenzaClasse=true;
		int flag=0;
		
		try {
			Class.forName(dbDriver);
			conn = DriverManager.getConnection(dbUrlDest,dbUsernameDest,dbPasswordDest);
			query = "SELECT 1 as flagAssenza FROM TIPOLOG_DCTM_ARCHIVIAZIONE WHERE ROWNUM = 1 AND RIF_CLASSE_DOC = ? ";
			
			ps = conn.prepareStatement(query);
			ps.setInt(1, lavorazioneId);
			log.info("Eseguo query: " + query + "("+lavorazioneId+")");

			rs = ps.executeQuery();		

	        while(rs.next()) {
	        	flag=rs.getInt("flagAssenza");
	        	flagAssenzaClasse=false; //classe presente, rec da non inserire
	        }
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeConnection(conn,rs,ps);
		}		
		
		//log.info("method VerificaAssenzaClasse ended");
		return flagAssenzaClasse;
	}
	*/
	
	/*
	private static List<String> EseguiUnmarshalling(String xmlForUnMarshalling, Properties appProperties) {
		//log.info("Method EseguiUnMarshalling started" );
		List<String> result = new ArrayList<String>();
		
        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(ObjectMapping.class);
            Unmarshaller jaxbUnM = jaxbContext.createUnmarshaller();
            StringReader reader = new StringReader(xmlForUnMarshalling);
            ObjectMapping objMap = (ObjectMapping) jaxbUnM.unmarshal(reader);
            
            int maxValues = objMap.getObjectFields().getObjectField().size();
            log.info("Valori attributi: " + maxValues);

            for (int i=0;i < maxValues;i++) {
            	result.add(objMap.getObjectFields().getObjectField().get(i).getSourceData().getSourceField());
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }        
		//log.info("Method EseguiUnMarshalling ended" );
		return result;
	}	
	*/
    
	/*
	private static String EseguiMarshalling(List<String> objectMapList) throws JAXBException {
		//log.info("Method EseguiMarshalling started con " + objectMapList);
		String xmlMarshalling="";
		
		JAXBContext jaxbContext = null;
		jaxbContext = org.eclipse.persistence.jaxb.JAXBContextFactory.createContext(new Class[]{Metadati.class}, null);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);		
		
		//valori da xml origine
		List<Metadati.Metadato> metadatoList = new ArrayList<Metadati.Metadato>();

		for (int ix=0; ix < objectMapList.size();ix++) {
			String value=objectMapList.get(ix);
			
			//Bypass valore object_id
			if (!(value.trim().toLowerCase().equals("object_id"))) {

				Metadati.Metadato.Property prop = new Metadati.Metadato.Property();
				Metadati.Metadato metadato = new Metadati.Metadato();
				
				prop.setName("name");
				prop.setValue(value);
			
				metadato.setProperty(prop);
				metadatoList.add(metadato);

			} else {
				log.info("Valore attributo: 'object_id' bypassato");
			}
		}
		Metadati mdati = new Metadati();
		mdati.setMetadato((ArrayList<Metadato>) metadatoList);
		
		//jaxbMarshaller.marshal(mdati, System.out);		
		
        //xml in formato string
        StringWriter writer = new StringWriter();
		jaxbMarshaller.marshal(mdati, writer);		
		xmlMarshalling = writer.toString();

		//log.info("Method EseguiMarshalling ended" );
		return xmlMarshalling;
	}
	*/
	

	//----------------------------------------------------------------------------------------------
	/*
	private static String CreaXml(List<String> objectMapList, Properties appProperties) {
		//https://www.journaldev.com/1112/how-to-write-xml-file-in-java-dom-parser
		log.info("method CreaXml Started");
		
		String xmlOutString=null;
		//String xmlFileOut=appProperties.getProperty("pathFileXml.produzioneOut");
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
		
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            doc.setXmlStandalone(true); //true=non mostra tag: standalone="no"?
            
            //add elements to Document
            Element rootElement = doc.createElementNS("", "metadati");
    
            //append root element to document
            doc.appendChild(rootElement);
            
            for (int ix=0;ix < objectMapList.size();ix++) {
            	String value=objectMapList.get(ix);
    
            	//append first child element to root element
                rootElement.appendChild(getMetadato(doc, value));
            }
            
            //for output to file, console
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            
            //for pretty print
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            
            //transformer.setOutputProperty(OutputKeys.STANDALONE, "no");
            transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, ""); //ritorni a capo
            
            DOMSource source = new DOMSource(doc);
            
            //write to console/file
            //StreamResult console = new StreamResult(System.out);
            //StreamResult file = new StreamResult(new File(xmlFileOut));
            //transformer.transform(source, file);
    
            //write data
            //transformer.transform(source, console);
    
            //xml in formato string
            StringWriter writer = new StringWriter();
            transformer.transform(source, new StreamResult(writer));
            xmlOutString = writer.toString();            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
		log.info("method CreaXml Ended");
        return xmlOutString;
	}
	*/
	
	/*
	private static Node getMetadato(Document doc, String value) {
		//log.info("method getMetadato Started");
        Element metadato = doc.createElement("metadato");
        //set id attribute
        //metadato.setAttribute("name", "name");
        //metadato.setAttribute("value", value);
    
        //create name element
        metadato.appendChild(getMetadatoElements(doc, metadato,"name",value));
    	//log.info("method getMetadato Ended");
        return metadato;
    }
	*/

    /*
	private static Node getMetadatoElements(Document doc, Element element,String name,String value) {
    	//log.info("method getMetadatoElements started");
        Element node = doc.createElement("property");
        node.setAttribute("name", name);
        node.setAttribute("value", value);
        //node.appendChild(doc.createTextNode("valore"));
    
    	//log.info("method getMetadatoElements Ended");
    	return node;
    }
	*/
	//----------------------------------------------------------------------------------------------

	
    public static void closeConnection(PreparedStatement ps, ResultSet rs, Connection cn  ) throws SQLException {
    	//log.info("Chiusura connessioni");
		if (!(rs==null)) {
			rs.close();
		}
		if (cn != null ) 
			cn.close();
		
		if (ps==null) {			
	       	ps.close();			
	       }			
    	log.info("Chiusura connessioni: OK");
    }


}
