package it.postel;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import it.postel.bean.AuditBean;
import it.postel.bean.ClasseDocumentaleBean;
import it.postel.bean.ClasseDocumentaleBean.TABLE_PARTITION;
import it.postel.bean.ErroreBean;
import it.postel.bean.MetadatiClasseDocBean;
import it.postel.bean.ParametriBean;
import it.postel.dao.DbDao;
//import jdk.internal.org.jline.utils.Log;
import jdk.internal.org.jline.utils.Log;

/* Lettura tabella SDC_NUOVE_CLASSI
 *
 * 		ID_CLASSE_MODELLO NUMBER(38,0), 
 * 		ZCODE VARCHAR2(8),
 * 		NUOVO_NOME_CLASSE VARCHAR2(600 BYTE),
 * 		FLAG_LAVORATO CHAR(1),  0=da lavorare, 1=lavorato, -1=errore, skip  
 * 		DATA_LAVORAZIONE 
 */

public class CreazioneClassiSdc {

	static Logger log = Logger.getLogger(CreazioneClassiSdc.class);

	// @SuppressWarnings({ "null", "restriction", "unused" })
	@SuppressWarnings("unused")
	public static void main(String[] args) throws Exception, ClassNotFoundException {
		// URL path = CreazioneClassiSdc.class.getClass().getResource("/");
		Connection cn = null, cnSdc = null;

		int totWrite = 0;
		AuditBean auditBean = null;
		List<ParametriBean> paramList = new ArrayList<ParametriBean>();
		ClasseDocumentaleBean classeDoc = null;
		ErroreBean erroreBean = null;

		// recupera ip
		InetAddress host = InetAddress.getLocalHost();

		String dbUrl = null, dbUsername = null, dbPassword = null, dbTabellaParametri = null, dbUrlSdc = null,
				dbUsernameSdc = null, dbPasswordSdc = null, msg = null, pathNew = null, ip = null, username = null,
				codiceErrore = null, pathIn = null, mapKey = null, mapValue = null,pathDaVerificare=null;
		int flagLavoratoNew=0,totClassiInsert=0,totClassiLette=0,totClassiScartate=0;
		boolean check = false, auditing = false, checkExistZeta = false, zetaInPathVersamento = false,
				zetaInPathConservazione = false, zetaInPathArchiviazione = false, esitoCreatePartition = false,
				flagOk = false,flagCreazionePath=false;
		long idAudit=0;
		
		Map<String, String> mapVerificaCreaPath = new HashMap<String, String>();
		Map.Entry<String, String> entry = null;

		try {

			// ------------------------------------------------------------------------------------------------------------------------------------------
			// File properties
			FileReader reader = null;
			reader = new FileReader("application.properties");
			Properties prop = new Properties();
			prop.load(reader);
			log.info("File properties: " + prop.toString());

			dbUrl = prop.getProperty("dbUrl");
			dbUsername = prop.getProperty("dbUsername");
			dbPassword = prop.getProperty("dbPassword");
			dbTabellaParametri = prop.getProperty("dbTabellaParametri");

			dbUrlSdc = prop.getProperty("dbUrlSdc");
			dbUsernameSdc = prop.getProperty("dbUsernameSdc");
			dbPasswordSdc = prop.getProperty("dbPasswordSdc");

			auditing = Boolean.parseBoolean(prop.getProperty("auditing"));

			// Apro Connessione DB catalogo
			cn = openConnection(dbUrl, dbUsername, dbPassword);

			// Apro Connessione DB Sdc
			cnSdc = openConnection(dbUrlSdc, dbUsernameSdc, dbPasswordSdc);
			// ------------------------------------------------------------------------------------------------------------------------------------------

			// ------------------------------------------------------------------------------------------------------------------------------------------
			log.info("Recupera parametri delle classi da creare (solo FLAG_LAVORATO=0)");
			paramList = DbDao.getParam(cn, dbTabellaParametri);
			if (paramList.isEmpty()) {
				log.info("Nessuna classe da creare");
				return;
			} else {
				log.info(paramList.size() + " classi da creare: " + paramList);
			}
			// ------------------------------------------------------------------------------------------------------------------------------------------

			
			// ------------------------------------------------------------------------------------------------------------------------------------------
			// recupera ip e host per audit
			ip = host.getHostAddress();
			username = (getUsernameLogged());
			username = "admin";
			log.info("ip / username: " + ip + " / " + username);
			// ------------------------------------------------------------------------------------------------------------------------------------------

			
			// ------------------------------------------------------------------------------------------------------------------------------------------
			// cicla sui parametri
			int iPar = 0;
			// for (ParametriBean param : paramList) {
			for (ParametriBean param : paramList) {

				log.info("");
				log.info("==============================================================================================");
				log.info("Analisi del parametro: " + param + " su " + paramList.size() + " parametri");

				flagLavoratoNew = 1;
				totClassiLette++;
				
				//valorizza AuditBean
				auditBean = new AuditBean(null, ip, username, AuditBean.TOOL_SDC.CREAZIONE_CLASSE_DOCUMENTALE.name(),null,
											AuditBean.TIPO_OGGETTO.CLASSE_DOCUMENTALE.name(), 
											"Creazione classe documentale da modello: " + param.getIdClasseModello(), false, null);
				log.info("auditBean: " + auditBean);

				
				log.info("Recupera dati classe: " + param.getIdClasseModello());
				classeDoc = DbDao.getClasseDocById(cnSdc, param.getIdClasseModello());
				
				if (classeDoc.getIdClasseDoc()==null) {
					erroreBean = new ErroreBean();
					erroreBean.setDescrizioneErrore("Classe di riferimento ("+param.getIdClasseModello()+") non estratta. Verificare TIPO_CLASSE_DOC (deve essere definitiva), NEXT PARAM");
					erroreBean.setFlagLavoratoNew(-1);
					erroreBean.setCodiceErrore("011");
					erroreBean.setPathNew(null);
					DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri, erroreBean);

					log.warn(erroreBean.getDescrizioneErrore());
					auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
					totClassiScartate++;
					continue;
				}
				log.info(classeDoc);
				//log.info("----------------------------------------");

				
				try {
					//log.info("----------------------------------------");
					//auditBean = new AuditBean(null, ip, username, AuditBean.TOOL_SDC.CREAZIONE_CLASSE_DOCUMENTALE.name(),
					//		String.valueOf(classeDoc.getIdClasseDoc()),
					//			AuditBean.TIPO_OGGETTO.CLASSE_DOCUMENTALE.name(), 
					//				"Creazione classe documentale da classe/modello: " + classeDoc.getIdClasseDoc()+"/"+classeDoc.getNomeClasseDocumentale(), true, null);
					//log.info("auditBean: " + auditBean);
					auditBean.setCodOggetto(String.valueOf(classeDoc.getIdClasseDoc()));
					auditBean.setDescrizione("Creazione classe documentale da classe/modello: " + classeDoc.getIdClasseDoc()+"/"+classeDoc.getNomeClasseDocumentale());
					
					// valorizza zcode
					classeDoc.setzCode(param.getZcode());
					
					//=========================================================================================
					pathDaVerificare=null;
					pathDaVerificare=classeDoc.getPathArchiviazione();
					if (!(pathDaVerificare == null)) {
						log.info("----------------------------------------");
						log.info("Verifico PATH_ARCHIVIAZIONE");

						//Verifico checkExistZeta
						checkExistZeta = checkExistZetaInPath(param.getIdClasseModello(), param.getZcode(),pathDaVerificare, dbTabellaParametri);
						if (checkExistZeta==false) {
							erroreBean = new ErroreBean();
							erroreBean.setDescrizioneErrore("Il path PATH_ARCHIVIAZIONE, non contiente uno zeta, (" +pathDaVerificare+"), NEXT PARAM");
							erroreBean.setFlagLavoratoNew(-1);
							erroreBean.setCodiceErrore("006");
							erroreBean.setPathNew(null);
							DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri, erroreBean);

							log.warn(erroreBean.getDescrizioneErrore());
							//auditBean.setCodOggetto(param.getIdClasseModello()+"/"+classeDoc.getIdClasseDoc());
							//auditBean.setEsito(false);
							auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
							totClassiScartate++;
							continue;
						}
						
						//Crea path
						pathNew = creaPath(pathDaVerificare, cn, param,dbTabellaParametri);
						if (pathNew==null) {
								erroreBean = new ErroreBean();
								erroreBean.setDescrizioneErrore("Path PATH_ARCHIVIAZIONE non creato (errore in creazione), non creo la classe, NEXT PARAM");
								erroreBean.setFlagLavoratoNew(-1);
								erroreBean.setCodiceErrore("001");
								erroreBean.setPathNew(null);
								DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri,erroreBean);

								log.warn(erroreBean.getDescrizioneErrore());
								//auditBean.setCodOggetto(param.getIdClasseModello()+"/"+classeDoc.getIdClasseDoc());
								//auditBean.setEsito(false);
								auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
								totClassiScartate++;
								continue;
						} 

						//ok
						classeDoc.setPathArchiviazione(pathNew);
						log.info("   > PATH_ARCHIVIAZIONE creato, esistente oppure mancante di zeta/null: "+ pathNew);
					} else {
						log.info("PATH_ARCHIVIAZIONE non presente in classe di riferimento, skip verifica/creazione path, proseguo con la creazione della classe");
					}
					//=========================================================================================
					
					
					
					//=========================================================================================
					// Verifica PATH_BACKUP_PDV (� sempre lo stesso per tutte le classi documentali)
					// Viene effettuato solo il controllo dell'assenza dello ZETA nella stringa 
					if (!(classeDoc.getPathBackupPdv() == null)) {
						log.info("----------------------------------------");
						log.info("Verifico PATH_BACKUP_PDV");
						
						checkExistZeta = checkExistZetaInPath(param.getIdClasseModello(), param.getZcode(),classeDoc.getPathBackupPdv(), dbTabellaParametri);
						if (checkExistZeta==true) {
							erroreBean = new ErroreBean();
							erroreBean.setDescrizioneErrore("Il path contiente uno zeta non atteso, (" +classeDoc.getPathBackupPdv()+"), NEXT PARAM");
							erroreBean.setFlagLavoratoNew(-1);
							erroreBean.setCodiceErrore("006");
							erroreBean.setPathNew(null);
							DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri, erroreBean);

							log.warn(erroreBean.getDescrizioneErrore());
							//auditBean.setEsito(false);
							auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
							totClassiScartate++;
							continue;
						} else {
							log.info("   > PATH_BACKUP_PDV: OK - " + classeDoc.getPathBackupPdv());
						}
					} else {
						log.info("PATH_BACKUP_PDV non presente in classe di riferimento, skip verifica/creazione path, proseguo con la creazione della classe");
					}
					//=========================================================================================

					
					// PATH_EMISSIONE -> no vericica controllo, � un campo ereditato dal vecchio sistema COD, non � utilizzato


					//=========================================================================================
					pathDaVerificare=null;
					pathDaVerificare=classeDoc.getPathIn();
					
					if (!(pathDaVerificare == null)) {
						//log.info("----------------------------------------");
						log.info("Verifico PATH_IN");

						//Verifico checkExistZeta
						checkExistZeta = checkExistZetaInPath(param.getIdClasseModello(), param.getZcode(),pathDaVerificare, dbTabellaParametri);
						if (checkExistZeta==false) {
							erroreBean = new ErroreBean();
							erroreBean.setDescrizioneErrore("Il path PATH_IN, non contiente uno zeta, (" +pathDaVerificare+"), NEXT PARAM");
							erroreBean.setFlagLavoratoNew(-1);
							erroreBean.setCodiceErrore("006");
							erroreBean.setPathNew(null);
							DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri, erroreBean);

							log.warn(erroreBean.getDescrizioneErrore());
							//auditBean.setEsito(false);
							//auditBean.setCodOggetto(String.valueOf(param.getIdClasseModello()));
							auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
							totClassiScartate++;
							continue;
						}
						
						//Crea path
						pathNew = creaPath(pathDaVerificare, cn, param,dbTabellaParametri);
						if (pathNew==null) {
								erroreBean = new ErroreBean();
								erroreBean.setDescrizioneErrore("Path PATH_IN non creato (errore in creazione), non creo la classe, NEXT PARAM");
								erroreBean.setFlagLavoratoNew(-1);
								erroreBean.setCodiceErrore("001");
								erroreBean.setPathNew(null);
								DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri,erroreBean);

								log.warn(erroreBean.getDescrizioneErrore());
								//auditBean.setCodOggetto(String.valueOf(param.getIdClasseModello()));
								//auditBean.setEsito(false);
								auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
								totClassiScartate++;
								continue;
						} 

						//ok
						classeDoc.setPathIn(pathNew);
						log.info("   > PATH_IN creato, esistente oppure mancante dello zeta/null: "+ pathNew); //se manca zeta, path=null
						
					} else {
						log.info("PATH_IN non presente in classe di riferimento, skip verifica/creazione path, proseguo con la creazione della classe");
					}
					//=========================================================================================
					
					
					
					//=========================================================================================
					pathDaVerificare=null;
					pathDaVerificare=classeDoc.getPathPrescarto();
					
					if (!(pathDaVerificare == null)) {
						log.info("----------------------------------------");
						log.info("Verifico PATH_PRESCARTO");

						//Verifico checkExistZeta
						checkExistZeta = checkExistZetaInPath(param.getIdClasseModello(), param.getZcode(),pathDaVerificare, dbTabellaParametri);
						if (checkExistZeta==false) {
							erroreBean = new ErroreBean();
							erroreBean.setDescrizioneErrore("Il path PATH_PRESCARTO, non contiente uno zeta, (" +pathDaVerificare+"), NEXT PARAM");
							erroreBean.setFlagLavoratoNew(-1);
							erroreBean.setCodiceErrore("006");
							erroreBean.setPathNew(null);
							DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri, erroreBean);

							log.warn(erroreBean.getDescrizioneErrore());
							//auditBean.setEsito(false);
							//auditBean.setCodOggetto(String.valueOf(param.getIdClasseModello()));
							auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
							totClassiScartate++;
							continue;
						}
						
						//Crea path
						pathNew = creaPath(pathDaVerificare, cn, param,dbTabellaParametri);
						if (pathNew==null) {
								erroreBean = new ErroreBean();
								erroreBean.setDescrizioneErrore("Path PATH_PRESCARTO non creato (errore in creazione), non creo la classe, NEXT PARAM");
								erroreBean.setFlagLavoratoNew(-1);
								erroreBean.setCodiceErrore("001");
								erroreBean.setPathNew(null);
								DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri,erroreBean);

								log.warn(erroreBean.getDescrizioneErrore());
								//auditBean.setCodOggetto(String.valueOf(param.getIdClasseModello()));
								//auditBean.setEsito(false);
								auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
								totClassiScartate++;
								continue;
						} 

						//ok
						classeDoc.setPathPrescarto(pathNew);
						log.info("   > PATH_PRESCARTO creato, esistente oppure mancante di zeta/null: "+ pathNew);
					} else {
						log.info("PATH_PRESCARTO non presente in classe di riferimento, skip verifica/creazione path, proseguo con la creazione della classe");
					}
					//=========================================================================================


					
					//=========================================================================================
					pathDaVerificare=null;
					pathDaVerificare=classeDoc.getPathScarto();
					
					if (!(pathDaVerificare == null)) {
						//log.info("----------------------------------------");
						log.info("Verifico PATH_SCARTO");

						//Verifico checkExistZeta
						checkExistZeta = checkExistZetaInPath(param.getIdClasseModello(), param.getZcode(),pathDaVerificare, dbTabellaParametri);
						if (checkExistZeta==false) {
							erroreBean = new ErroreBean();
							erroreBean.setDescrizioneErrore("Il path PATH_SCARTO, non contiente uno zeta, (" +pathDaVerificare+"), NEXT PARAM");
							erroreBean.setFlagLavoratoNew(-1);
							erroreBean.setCodiceErrore("006");
							erroreBean.setPathNew(null);
							DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri, erroreBean);

							log.warn(erroreBean.getDescrizioneErrore());
							//auditBean.setCodOggetto(String.valueOf(param.getIdClasseModello()));
							//auditBean.setEsito(false);
							auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
							totClassiScartate++;
							continue;
						}
						
						//Crea path
						pathNew = creaPath(pathDaVerificare, cn, param,dbTabellaParametri);
						if (pathNew==null) {
								erroreBean = new ErroreBean();
								erroreBean.setDescrizioneErrore("Path PATH_SCARTO non creato (errore in creazione), non creo la classe, NEXT PARAM");
								erroreBean.setFlagLavoratoNew(-1);
								erroreBean.setCodiceErrore("001");
								erroreBean.setPathNew(null);
								DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri,erroreBean);

								log.warn(erroreBean.getDescrizioneErrore());
								//auditBean.setCodOggetto(String.valueOf(param.getIdClasseModello()));
								//auditBean.setEsito(false);
								auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
								totClassiScartate++;
								continue;
						} 

						//ok
						classeDoc.setPathScarto(pathNew);
						log.info("   > PATH_SCARTO creato, esistente oppure mancante di zeta/null: "+ pathNew);
						
					} else {
						log.info("PATH_SCARTO non presente in classe di riferimento, skip verifica/creazione path, proseguo con la creazione della classe");
					}
					//=========================================================================================
					
					
					//=========================================================================================
					// Recupera dati per classe reportClienti, se null, NEXT PARAM
					log.info("----------------------------------------");
					log.info("Recupera dati per classe reportClienti");
					classeDoc.setReportClasseDoc(DbDao.getReportClientiByIdClasseDoc(cnSdc, param.getIdClasseModello()));
					
					// se non vi sono dati, NEXT PARAM
					if (classeDoc.getReportClasseDoc() == null) {
						erroreBean = new ErroreBean();
						erroreBean.setDescrizioneErrore("Nessuna classe modello in STORAGE_REPORT_CLIENTI_CLASSE_DOC, NEXT PARAM");
						erroreBean.setFlagLavoratoNew(-1);
						erroreBean.setCodiceErrore("002");
						erroreBean.setPathNew("");
						DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri,erroreBean);

						log.warn(erroreBean.getDescrizioneErrore());
						//auditBean.setCodOggetto(String.valueOf(param.getIdClasseModello()));
						//auditBean.setEsito(false);
						auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
						totClassiScartate++;
						continue;
					} else {
						log.info("Dati STORAGE_REPORT_CLIENTI_CLASSE_DOC: " + classeDoc.getReportClasseDoc());
					}
					//=========================================================================================

					
					
					//=========================================================================================
					pathDaVerificare=null;
					pathDaVerificare=classeDoc.getReportClasseDoc().getPathRapportoArchiviazione();
					
					if (!(pathDaVerificare == null)) {
						log.info("----------------------------------------");
						log.info("Verifico PATH_RAPPORTO_ARCHIVIAZIONE");

						//Verifico checkExistZeta
						checkExistZeta = checkExistZetaInPath(param.getIdClasseModello(), param.getZcode(),pathDaVerificare, dbTabellaParametri);
						if (checkExistZeta==false) {
							erroreBean = new ErroreBean();
							erroreBean.setDescrizioneErrore("Il path PATH_RAPPORTO_ARCHIVIAZIONE, non contiente uno zeta, (" +pathDaVerificare+"), NEXT PARAM");
							erroreBean.setFlagLavoratoNew(-1);
							erroreBean.setCodiceErrore("006");
							erroreBean.setPathNew(null);
							DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri, erroreBean);

							log.warn(erroreBean.getDescrizioneErrore());
							//auditBean.setCodOggetto(String.valueOf(param.getIdClasseModello()));
							//auditBean.setEsito(false);
							auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
							totClassiScartate++;
							continue;
						}
						
						//Crea path
						pathNew = creaPath(pathDaVerificare, cn, param,dbTabellaParametri);
						if (pathNew==null) {
								erroreBean = new ErroreBean();
								erroreBean.setDescrizioneErrore("Path PATH_RAPPORTO_ARCHIVIAZIONE non creato (errore in creazione), non creo la classe, NEXT PARAM");
								erroreBean.setFlagLavoratoNew(-1);
								erroreBean.setCodiceErrore("001");
								erroreBean.setPathNew(null);
								DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri,erroreBean);

								log.warn(erroreBean.getDescrizioneErrore());
								//auditBean.setCodOggetto(String.valueOf(param.getIdClasseModello()));
								//auditBean.setEsito(false);
								auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
								totClassiScartate++;
								continue;
						} 

						//ok
						classeDoc.getReportClasseDoc().setPathRapportoArchiviazione(pathNew);
						log.info("   > PATH_RAPPORTO_ARCHIVIAZIONE creato, esistente oppure mancante di zeta/null: "+ pathNew);
						
					} else {
						log.info("PATH_RAPPORTO_ARCHIVIAZIONE non presente in classe di riferimento, skip verifica/creazione path, proseguo con la creazione della classe");
					}
					//=========================================================================================


					
					//=========================================================================================
					pathDaVerificare=null;
					pathDaVerificare=classeDoc.getReportClasseDoc().getPathRapportoArchiviazione();
					
					if (!(pathDaVerificare == null)) {
						log.info("----------------------------------------");
						log.info("Verifico PATH_RAPPORTO_VERSAMENTO");

						//Verifico checkExistZeta
						checkExistZeta = checkExistZetaInPath(param.getIdClasseModello(), param.getZcode(),pathDaVerificare, dbTabellaParametri);
						if (checkExistZeta==false) {
							erroreBean = new ErroreBean();
							erroreBean.setDescrizioneErrore("Il path PATH_RAPPORTO_VERSAMENTO, non contiente uno zeta, (" +pathDaVerificare+"), NEXT PARAM");
							erroreBean.setFlagLavoratoNew(-1);
							erroreBean.setCodiceErrore("006");
							erroreBean.setPathNew(null);
							DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri, erroreBean);

							log.warn(erroreBean.getDescrizioneErrore());
							//auditBean.setCodOggetto(String.valueOf(param.getIdClasseModello()));
							//auditBean.setEsito(false);
							auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
							totClassiScartate++;
							continue;
						}
						
						//Crea path
						pathNew = creaPath(pathDaVerificare, cn, param,dbTabellaParametri);
						
						
						if (pathNew==null) {
								erroreBean = new ErroreBean();
								erroreBean.setDescrizioneErrore("Path PATH_RAPPORTO_VERSAMENTO non creato (errore in creazione), non creo la classe, NEXT PARAM");
								erroreBean.setFlagLavoratoNew(-1);
								erroreBean.setCodiceErrore("001");
								erroreBean.setPathNew(null);
								DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri,erroreBean);

								log.warn(erroreBean.getDescrizioneErrore());
								//auditBean.setCodOggetto(String.valueOf(param.getIdClasseModello()));
								//auditBean.setEsito(false);
								auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
								totClassiScartate++;
								continue;
						} 

						//ok
						classeDoc.getReportClasseDoc().setPathRapportoVersamento(pathNew);
						log.info("   > PATH_RAPPORTO_VERSAMENTO creato, esistente oppure mancante di zeta/null: "+ pathNew);
						
					} else {
						log.info("PATH_RAPPORTO_VERSAMENTO non presente in classe di riferimento, skip verifica/creazione path, proseguo con la creazione della classe");
					}
					//=========================================================================================
					

					log.info("----------------------------------------");
					classeDoc.setReportFtp(DbDao.getReportClientiFtpByIdClasseDoc(cnSdc, param.getIdClasseModello()));
					log.info("classeDoc.getReportFtp(): " + classeDoc.getReportFtp());
					//log.info("----------------------------------------");

					
					log.info("----------------------------------------");
					classeDoc.setReportPdd(DbDao.getReportPddByIdClasseDoc(cnSdc, param.getIdClasseModello()));
					log.info("classeDoc.getReportPdd(): " + classeDoc.getReportPdd());
					//log.info("----------------------------------------");

					
					log.info("----------------------------------------");
					classeDoc.setListaMetadati(DbDao.getMetadatiClasseDocByIdClasseDoc(cnSdc, param.getIdClasseModello()));
					log.info("ListaMetadati(): " + classeDoc.getListaMetadati());
					//log.info("----------------------------------------");

					
					log.info("----------------------------------------");
					log.info("Recupera nome classe documentale e descrizione");
					String nomeProcedura = DbDao.getProcedura(cnSdc, classeDoc.getRifProcedura());
					if (!(nomeProcedura == null)) {
						classeDoc.setDescrizione(param.getZcode() + "_" + nomeProcedura);
						classeDoc.setNomeClasseDocumentale(param.getZcode() + "_" + nomeProcedura);
					} else {
						erroreBean=new ErroreBean();
						erroreBean.setCodiceErrore("004");
						erroreBean.setDescrizioneErrore("NOME_PROCEDURA non presente in TIPOLOG_PROCEDURA, NEXT PARAM");
						erroreBean.setFlagLavoratoNew(-1);
						erroreBean.setPathNew(null);
						DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(), dbTabellaParametri,erroreBean);

						log.warn(erroreBean.getDescrizioneErrore());
						//auditBean.setCodOggetto(String.valueOf(param.getIdClasseModello()));
						//auditBean.setEsito(false);
						auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
						totClassiScartate++;
						continue;
					}
					log.info("nomeClasseDocumentale: " + classeDoc.getNomeClasseDocumentale());
					// ============================================================================================

					
					
					// ============================================================================================
					// INSERT CLASSE
					// ============================================================================================
					//try {
						log.info("----------------------------------------");
						log.info("insertClasseDocumentale: " + classeDoc );

						cnSdc.setAutoCommit(false);
						Integer idClasseDoc = DbDao.insertClasseDocumentale(cnSdc, classeDoc);

						if (idClasseDoc == null) {
							cnSdc.rollback();

							erroreBean.setPathNew(null);
							erroreBean.setFlagLavoratoNew(-1);
							erroreBean.setCodiceErrore("005");
							erroreBean.setDescrizioneErrore("Errore in INSERT classe " + param.getIdClasseModello() + ", NEXT PARAM");

							DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(),dbTabellaParametri, erroreBean);
							cn.commit();
							
							log.warn(erroreBean.getDescrizioneErrore());
							//auditBean.setCodOggetto(param.getIdClasseModello()+"/"+idClasseDoc);
							//auditBean.setEsito(false);
							auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
							totClassiScartate++;
							continue;
						}
						//log.info("----------------------------------------");

						
						//valorizza idClasse appena creato
						classeDoc.setIdClasseDoc(idClasseDoc);
						
						
						log.info("----------------------------------------");
						log.info("Crea partizioni per partition_name per nuova classe " +classeDoc.getIdClasseDoc());

						//Per le classi definitive inserisco le partizioni
						String partitionName = classeDoc.getNomeClasseDocumentale().toUpperCase();
						Integer partitionId = idClasseDoc;

						log.info("   > partitionName="+partitionName+", partitionId="+partitionId);
						createPartitionForClasseDoc(cnSdc, partitionName, partitionId, classeDoc);
						//log.info("----------------------------------------");

						
						
						log.info("----------------------------------------");
						log.info("INSERT INTO STORAGE_REPORT_CLIENTI_CLASSE_DOC " + classeDoc.getReportClasseDoc()+" per nuova classe " +  classeDoc.getIdClasseDoc());
						Integer idReportClienti = DbDao.insertReportClientiClasseDoc(cnSdc,classeDoc );

						if (idReportClienti == null) {
							cnSdc.rollback();

							erroreBean.setPathNew(null);
							erroreBean.setFlagLavoratoNew(-1);
							erroreBean.setCodiceErrore("007");
							erroreBean.setDescrizioneErrore("Errore in INSERT STORAGE_REPORT_CLIENTI_CLASSE_DOC "+ param.getIdClasseModello() + ", NEXT PARAM");

							DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(),dbTabellaParametri, erroreBean);
							cnSdc.commit();
							
							log.warn(erroreBean.getDescrizioneErrore());
							//auditBean.setCodOggetto(param.getIdClasseModello()+"/"+idClasseDoc);
							//auditBean.setEsito(false);
							auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
							totClassiScartate++;
							continue;
						}
						//log.info("----------------------------------------");

						
						log.info("----------------------------------------");
						log.info("INSERT INTO STORAGE_REPORT_CLIENTI_FTP " + classeDoc.getReportFtp()+" per nuova classe " +  classeDoc.getIdClasseDoc());
						if (classeDoc.getReportFtp() != null) {
							log.info("ReportFtp con valore presente: " + classeDoc.getReportFtp() + " per nuova classe: " + classeDoc.getIdClasseDoc());
							
							Integer idReportFtp = DbDao.insertReportFtpClasseDoc(cnSdc, classeDoc);
							if (idReportFtp == null) {
								cnSdc.rollback();
								
								erroreBean = new ErroreBean();
								erroreBean.setPathNew(null);
								erroreBean.setFlagLavoratoNew(-1);
								erroreBean.setCodiceErrore("007");
								erroreBean.setDescrizioneErrore("Errore in INSERT STORAGE_REPORT_CLIENTI_FTP, classe di riferimento: " + param.getIdClasseModello() + ", NEXT PARAM");

								DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(),dbTabellaParametri, erroreBean);
								cnSdc.commit();
								
								log.warn(erroreBean.getDescrizioneErrore());
								//auditBean.setCodOggetto(param.getIdClasseModello()+"/"+idClasseDoc);
								//auditBean.setEsito(false);
								auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
								totClassiScartate++;
								continue;
							}
							classeDoc.getReportFtp().setIdReportClientiFtp(idReportFtp);
						}
						//log.info("----------------------------------------");
						

						
						log.info("----------------------------------------");
						log.info("INSERT INTO STORAGE_REPORT_PDD_CLASSSE_DOC " + classeDoc.getReportPdd()+" per nuova classe " +  classeDoc.getIdClasseDoc());
						if (classeDoc.getReportPdd() != null) {
							log.info("ReportPdd "+classeDoc.getReportPdd()+" per nuova classe " + classeDoc.getIdClasseDoc());

							Integer idReportPdd = DbDao.insertReportPddClasseDoc(cnSdc, classeDoc);
							if (idReportPdd == null) {
								cnSdc.rollback();
								
								erroreBean=new ErroreBean();
								erroreBean.setPathNew(null);
								erroreBean.setFlagLavoratoNew(-1);
								erroreBean.setCodiceErrore("007");
								erroreBean.setDescrizioneErrore("Errore in INSERT STORAGE_REPORT_PDD_CLASSSE_DOC "+ param.getIdClasseModello() + ", NEXT PARAM");
								DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(),dbTabellaParametri, erroreBean);
								cnSdc.commit();

								log.warn(erroreBean.getDescrizioneErrore());
								//auditBean.setCodOggetto(param.getIdClasseModello()+"/"+idClasseDoc);
								//auditBean.setEsito(false);
								auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
								totClassiScartate++;
								continue;
							}

							classeDoc.getReportPdd().setIdReportPdd(idReportPdd);
						}
						//log.info("----------------------------------------");

						
						log.info("----------------------------------------");
						flagOk = false;
						if (classeDoc.getListaMetadati() != null && classeDoc.getListaMetadati().size() > 0) {
							log.info("Insert dei metadati nella TIPOLOG_METADATI_CLASSE_DOC");
							int im = 0;
							boolean forOk=true;
							
							for (MetadatiClasseDocBean metaBean : classeDoc.getListaMetadati()) {
								metaBean.setRifClasseDoc(idClasseDoc);
								Integer idMetadato = DbDao.insertMetadatoClasseDoc(cnSdc, metaBean, idClasseDoc);

								if (idMetadato == null) {
									cnSdc.rollback();
									
									erroreBean=new ErroreBean();
									erroreBean.setPathNew(null);
									erroreBean.setFlagLavoratoNew(-1);
									erroreBean.setCodiceErrore("008");
									erroreBean.setDescrizioneErrore("Errore INSERT METADATO in TIPOLOG_METADATI_CLASSE_DOC "+ param.getIdClasseModello() + ", NEXT PARAM");
									DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(),dbTabellaParametri, erroreBean);
									cnSdc.commit();

									log.warn(erroreBean.getDescrizioneErrore());
									//auditBean.setCodOggetto(param.getIdClasseModello()+"/"+idClasseDoc);
									//auditBean.setEsito(false);
									forOk=false;
									break;
								}
								metaBean.setIdMetadato(idMetadato);
								im++;
							}
							
							//se la insert di un metadato va in errore, salta la classe e va al prossimo param/classe da creare  
							if ( !(forOk)) {
								//log.info("appena uscito dal for (insertMetadatoClasseDoc) e passo al prossimo param");
								auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
								totClassiScartate++;
								continue;
							}

							log.info("Metadati inseriti: " + im + ", previsti " + classeDoc.getListaMetadati().size());
						}
						//log.info("----------------------------------------");


						// inserimento associazione classe doc utenteAmministratore
						log.info("----------------------------------------");
						List<String> names = DbDao.getListUsernameAmministratori(cnSdc);
						
						//for(String a : names) {
						//	log.info(a);
						//}

						log.info("Inserimento associazione classe doc utenteAmministratore, recs da inserire: " + names.size());
						int in = 0;
						boolean forOk=true;
						
						for (String name : names) {
							Integer result = DbDao.insertClasseDocAssociation(cnSdc, name, idClasseDoc, username);
							if (result == null) {
								cnSdc.rollback();
								
								erroreBean=new ErroreBean();
								erroreBean.setPathNew(null);
								erroreBean.setFlagLavoratoNew(-1);
								erroreBean.setCodiceErrore("009");
								erroreBean.setDescrizioneErrore("Errore INSERT SDC_USR_CLASSE_DOC " +param.getIdClasseModello()+", username: " + username+", NEXT PARAM");
								DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(),dbTabellaParametri, erroreBean);
								cn.commit();

								log.warn(erroreBean.getDescrizioneErrore());
								//auditBean.setCodOggetto(param.getIdClasseModello()+"/"+idClasseDoc);
								//auditBean.setEsito(false);
								forOk=false;
								break;
							}
							in++;
						}
						
						if (!(forOk)) {
							//se la insert di un elemento va in errore, salta la classe e va al prossimo param/classe da creare  
							log.info("appena uscito dal for (insertClasseDocAssociation) e passo al prossimo param");
							auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
							totClassiScartate++;
							continue;
						} 
						log.info("Inserimento associazione classe doc utenteAmministratore, recs inseriti: "+ names.size());

						if (!names.contains(username)) {
							// inserimento associazione classe doc utente
							log.info("inserimento associazione classe doc utente: " + username + ", " + idClasseDoc+ ", " + username);
							DbDao.insertClasseDocAssociation(cnSdc, username, idClasseDoc, username);
						}
						log.info("----------------------------------------");

						
						log.info("Eseguo commit nuova classe " + classeDoc.getIdClasseDoc()+", riferimento: " + param.getIdClasseModello()); 
						cnSdc.commit();
						totClassiInsert++;

						//Aggiorna tabella parametri
						erroreBean = new ErroreBean();
						erroreBean.setPathNew(null);
						erroreBean.setFlagLavoratoNew(1);
						erroreBean.setCodiceErrore("099");
						erroreBean.setDescrizioneErrore("Nuova classe (" +classeDoc.getIdClasseDoc()+") creata su modello ("+ param.getIdClasseModello() + ")");

						DbDao.aggiornaTabParametri(cn, param.getIdClasseModello(), param.getZcode(),dbTabellaParametri, erroreBean);
						cnSdc.commit();
						
						//auditBean.setCodOggetto(param.getIdClasseModello()+"/"+idClasseDoc);
						auditBean.setDescrizione(erroreBean.getDescrizioneErrore());
						auditBean.setEsito(true);
					// ============================================================================================

					log.info("Fine Analisi del parametro " + param.getIdClasseModello()+", "+param.getZcode());

				} catch (Throwable e) {
					log.info("catch (Throwable e)" );
					
					if(auditing) {
						//log.info("-----------------------------------------------------");
						auditBean.setEsito(false);
						auditBean.setDescrizione("Errore " + auditBean.getDescrizione()+". Classi create " + totClassiInsert);
						cnSdc.setAutoCommit(true);
						
						idAudit=DbDao.insertAudit(cnSdc, username, auditBean);
						//cnSdc.commit();
						if ( idAudit==0 ) {
							log.info("Insert SDC_AUDIT fallita!!!");
						} else {
							log.info("Insert SDC_AUDIT OK con ID " + idAudit);
						}
						log.info("-----------------------------------------------------");
						
					}
					throw new Exception(e);
				}

				iPar++;

				log.info("----------------------------------------------------------------------------------------------");
			} // fine ciclo lettura tabella zeta/modelli
			log.info("==============================================================================================");

			
			//Aggiorna auditing
			if (auditing) {
				//log.info("-----------------------------------------------------");
				
				auditBean.setEsito(true);
				auditBean.setDescrizione("Creazione masssiva classe documentale"); // da classe/modello: " + classeDoc.getIdClasseDoc()+"/"+classeDoc.getNomeClasseDocumentale());
				
				cnSdc.setAutoCommit(true);
				idAudit=DbDao.insertAudit(cnSdc, username, auditBean);
				if ( idAudit==0 ) {
					log.info("Insert SDC_AUDIT fallita!!!");
				} else {
					log.info("Insert SDC_AUDIT OK con ID " + idAudit);
				}
				log.info("-----------------------------------------------------");
			}
			
			//log.info("Fine FOR parametri");
			
			log.info("Statistiche");
			log.info("Classi Lette da param: " + totClassiLette);
			log.info("Nuove Classi inserite: " + totClassiInsert);
			log.info("Nuove Classi scartate: " + totClassiScartate);
			

		} catch (IOException ex) {
			ex.printStackTrace();

		} finally {
			closeConnection(null, null, cn);
		}
	}
	
	private static String creaPath(String pathIn, Connection cn, ParametriBean param,String dbTabellaParametri) throws SQLException {
		//log.info("started method creaPath");
		String pathNew = null;

		if (!(pathIn==null)) {
			//sostituisce zeta e ritorna nuovo path
			pathNew = getNewPath(pathIn, param.getZcode());
			
			if ( !(pathNew==null)) {
		        File newFolder = new File(pathNew); // Crea un oggetto File con il percorso specificato

		        if (newFolder.exists()) {
		            log.info("La cartella " + pathNew + " e' gi� esistente");
		        } else {
			        boolean result = newFolder.mkdirs(); // Crea la cartella 
			        
			        // Verifica se la creazione � avvenuta con successo
			        if (result) {
			            log.info("La cartella "+pathNew+" e' stata creata con successo.");
			        } else {
			            log.warn("Si � verificato un errore durante la creazione della cartella " + pathNew);
			        }
		        }
			} else {
				log.info("Nome Nuovo Path non creato");
			}
		}
		//log.info("ended method creaPath");
		return pathNew;
	}
	
	
	public static String getUsernameLogged() throws NoSuchMethodException, SecurityException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException {
		String osName = System.getProperty("os.name").toLowerCase();
		String className = null;
		String methodName = "getUsername";
		String username = null;

		if (osName.contains("windows")) {
			className = "com.sun.security.auth.module.NTSystem";
			methodName = "getName";
		} else if (osName.contains("linux")) {
			className = "com.sun.security.auth.module.UnixSystem";
		} else if (osName.contains("solaris") || osName.contains("sunos")) {
			className = "com.sun.security.auth.module.SolarisSystem";
		}

		if (className != null) {
			Class<?> c;
			c = Class.forName(className);
			Method method = c.getDeclaredMethod(methodName);
			Object o = c.newInstance();
			username = (String) method.invoke(o);
		}
		return username;
	}

	
	private static int createPartitionForClasseDoc(Connection cnSdc, String partitionName, Integer partitionId,
			ClasseDocumentaleBean classeDoc) throws SQLException {
		// log.info("started method createPartitionForClasseDoc");
		int result = 0;

		for (TABLE_PARTITION tableName : TABLE_PARTITION.values()) {
			//log.info("=============================");
			String tablespaceName = DbDao.getTablespaceName(cnSdc, tableName.name());

			// per la tabella tracking_ged le partizioni vanno cfeate solo se flag trackingGed= true
			//log.info("TRACKING_GED.name(): " + TABLE_PARTITION.TRACKING_GED.name() + ", isFlagTrackingGed(): "+ classeDoc.isFlagTrackingGed());
			
			if (classeDoc != null && (!tableName.name().equalsIgnoreCase(TABLE_PARTITION.TRACKING_GED.name())
					|| classeDoc.isFlagTrackingGed()
							&& tableName.name().equalsIgnoreCase(TABLE_PARTITION.TRACKING_GED.name()))) {

				// se la partition non esiste per tabella viene creata
				//log.info("se la partition non esiste per tabella viene creata");

				//log.info("checkIfPartitionExistsOnTable: "+ DbDao.checkIfPartitionExistsOnTable(cnSdc, partitionName, tableName.name()));

				if (!DbDao.checkIfPartitionExistsOnTable(cnSdc, partitionName, tableName.name())) {

					//log.info("partitionName=" + partitionName + ", partitionId=" + partitionId + ", tablespaceName=" + tablespaceName);
					//result += DbDao.createPartition(cnSdc, tableName.name(), partitionName, partitionId,tablespaceName);
					// Log.info("result + partizione creata: " + result);
					//log.info(result + " partizioni create");
				}
			} else {
				//log.info("Partizione da non creare ");
			}
			//log.info("=============================");
		}

		log.info("partizioni create: " + result);
		// log.info("ended method createPartitionForClasseDoc");

		return result;
	}

	
	private static boolean checkExistZetaInPath(int idClasseModello, String zcode, String path, String dbTabellaParametri ) throws SQLException {
		// log.info("started method checkExistZetaInPath: " + path);
		boolean result = false;
		ErroreBean erroreBean = new ErroreBean();
		
		if (path == null) {
			// log.error("Path '" + path + "' = NULL");
			return false;
		} else {
			if (path.toUpperCase().trim().contains("/Z")) {
				result = true;
			} else {
				// log.error("Path '" + path + "' senza ZETA");
				result = false;
			}
		}
		// log.info("ended method checkExistZetaInPath: " + result);
		return result;
	}

	
	private static String getNewPath(String path, String zcode) {
		//log.info("started method getNewPath: " + path);
		String pathNew = null;
		int iFlag = 0;

		if (path.length() == 0) {
			log.error("Stringa path NULL: --" + path + "--");
			return pathNew;
		} else {
			// elabora stringa e sostituisce zeta esistente con nuovo param
			String a[] = path.split("/");
			int i = 1;
			for (String b : a) {
				if (b.length() > 0) {
					if (b.substring(0, 1).toUpperCase().equals("Z")) {
						a[i] = zcode;
						iFlag++;
					}
					i++;
				}
			}
			if (iFlag > 0) {
				pathNew = String.join("/", a);
			}
		}
		//log.info("ended method getNewPath: " + pathNew);
		return pathNew;
	}

	
	private static Connection openConnection(String jdbcstring, String jdbcusername, String jdbcpassword)
			throws InterruptedException {
		int tentativoConnessioneDB = 1;
		Connection connection = null;
		while (tentativoConnessioneDB <= 5) {
			try {
				log.info("Tentativo Connessione DB numero " + tentativoConnessioneDB);
				connection = DriverManager.getConnection(jdbcstring, jdbcusername, jdbcpassword);
				log.info("Connessione DB: OK");
				break;
			} catch (Exception e) {
				log.warn("Errore connessione DB");
				log.warn(e.getMessage());
				Thread.sleep(5000);
				tentativoConnessioneDB++;
			}
		}
		if (tentativoConnessioneDB > 5) {
			log.warn("Errore connessione DB");
		}
		return connection;
	}

	
	public static void closeConnection(PreparedStatement ps, ResultSet rs, Connection cn) throws SQLException {
		// log.info("Chiusura connessioni");
		if (rs != null) {
			rs.close();
		}
		if (cn != null)
			cn.close();

		if (ps != null) {
			ps.close();
		}
		log.info("Chiusura connessioni: OK");
	}

}
