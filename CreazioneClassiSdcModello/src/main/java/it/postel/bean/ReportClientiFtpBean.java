package it.postel.bean;

import java.io.Serializable;

public class ReportClientiFtpBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Integer idReportClientiFtp;
	protected long rifClasseDoc;
	protected String ipServerFtp;
	protected String portServerFtp;
	protected String userNameFtp;
	protected String passwordFtp;
	
	
	
	@Override
	public String toString() {
		return "ReportClientiFtpBean [idReportClientiFtp=" + idReportClientiFtp + ", rifClasseDoc=" + rifClasseDoc
				+ ", ipServerFtp=" + ipServerFtp + ", portServerFtp=" + portServerFtp + ", userNameFtp=" + userNameFtp
				+ ", passwordFtp=" + passwordFtp + "]";
	}
	public Integer getIdReportClientiFtp() {
		return idReportClientiFtp;
	}
	public void setIdReportClientiFtp(Integer idReportClientiFtp) {
		this.idReportClientiFtp = idReportClientiFtp;
	}
	public long getRifClasseDoc() {
		return rifClasseDoc;
	}
	public void setRifClasseDoc(long rifClasseDoc) {
		this.rifClasseDoc = rifClasseDoc;
	}
	public String getIpServerFtp() {
		return ipServerFtp;
	}
	public void setIpServerFtp(String ipServerFtp) {
		this.ipServerFtp = ipServerFtp;
	}
	public String getPortServerFtp() {
		return portServerFtp;
	}
	public void setPortServerFtp(String portServerFtp) {
		this.portServerFtp = portServerFtp;
	}
	public String getUserNameFtp() {
		return userNameFtp;
	}
	public void setUserNameFtp(String userNameFtp) {
		this.userNameFtp = userNameFtp;
	}
	public String getPasswordFtp() {
		return passwordFtp;
	}
	public void setPasswordFtp(String passwordFtp) {
		this.passwordFtp = passwordFtp;
	}
}
