package it.postel.bean;

import java.io.Serializable;
import java.util.Date;

public class AuditBean implements Serializable{
	
	public static enum TIPO_OGGETTO {CLASSE_DOCUMENTALE/*,CLIENTE,UTENTE, PROFILO, DOCUMENTO,PDA,PDV,STORICO_JOB, PARAMETRI_JOB,TMP_FILES,SERVIZIO,JOB,EVENTO, ESIBIZIONE_MASSIVA, CONTROLLO_SEQUENZIALITA, ANNULLAMENTO, PACCHETTO_SMISTAMENTO, CERTIFICATO,FIRMATARI, ESIBIZIONE_GRANDI_DIMENSIONI, MODELLO_AGID*/}
	
	public static enum TOOL_SDC {CREAZIONE_CLASSE_DOCUMENTALE
		//INSERIMENTO_CLASSE_DOCUMENTALE
		//LOGIN,MODIFICA_PASSWORD,INSERIMENTO_UTENTE,RICERCA_UTENTE,EDIT_UTENTE,MODIFICA_UTENTE,ELIMINA_UTENTE,
		//INSERIMENTO_PROFILO,RICERCA_PROFILO,EDIT_PROFILO,MODIFICA_PROFILO,ELIMINA_PROFILO,INSERIMENTO_CLIENTE,
		//RICERCA_CLIENTE,EDIT_CLIENTE,MODIFICA_CLIENTE,INSERIMENTO_CLASSE_DOCUMENTALE,
		//RICERCA_CLASSE_DOCUMENTALE,EDIT_CLASSE_DOCUMENTALE,ELIMINA_CLASSE_DOCUMENTALE,
		//COPIA_CLASSE_DOCUMENTALE,ATTIVA_CLASSE_DOCUMENTALE,DISATTIVA_CLASSE_DOCUMENTALE,
		//MODIFICA_CLASSE_DOCUMENTALE,RICERCA_JOB,RISOTTOMETTERE_JOB,ELIMINA_FILE_TMP,
		//VISUALIZZA_STORICO_JOB,VISUALIZZA_PARAMETRI_JOB,RICERCA_SERVIZI,AVVIA_SERVIZIO,
		//ARRESTA_SERVIZIO,RICERCA_PDV,VISUALIZZA_DOCUMENTI_PDV,DOWNLOAD_DOCUMENTO,
		//DOWNLOAD_PDA_FIRMATO,DOWNLOAD_PDA_FIRMATO_MARCATO,ESIBIZIONE_RICERCA_DOCUMENTI,
		//AUDIT_RICERCA, RICARICA_CONFIGURAZIONI, INSERIMENTO_RICHIESTA_ESIBIZIONE, RICERCA_RICHIESTA_ESIBIZIONE,
		//EDIT_RICHIESTA_PDD, MODIFICA_RICHIESTA_PDD, AGGIUNGI_DOCUMENTI_RICHIESTA_PDD, ELIMINA_RICHIESTA_PDD, ELIMINA_DOCUMENTO_RICHIESTA_PDD,
		//INSERIMENTO_RICHIESTA_PDD_CLIENTE_CESSATO,EDIT_RICHIESTA_PDD_CLIENTE_CESSATO,CESSAZIONE_CLIENTE,RICERCA_CONTROLLO_SEQUENZIALITA,RIESEGUIRE_CONTROLLO_SEQUENZIALITA,DETTAGLIO_CONTROLLO_SEQUENZIALITA,
		//INSERIMENTO_RICHIESTA_ANNULLAMENTO, RICERCA_RICHIESTA_ANNULLAMENTO, ANNULLAMENTO_RICERCA_DOCUMENTI,
		//EDIT_RICHIESTA_ANNULLAMENTO, MODIFICA_RICHIESTA_ANNULLAMENTO, AGGIUNGI_DOCUMENTI_RICHIESTA_ANNULLAMENTO, ELIMINA_RICHIESTA_ANNULLAMENTO, ELIMINA_DOCUMENTO_RICHIESTA_ANNULLAMENTO, RICERCA_SMISTAMENTO,
		//VISUALIZZA_DOCUMENTI_ANNULLATI_PDV,SCARTO_DOCUMENTI_CLIENTE_CESSATO,RICERCA_SCARTO, VISUALIZZA_DOCUMENTI_SCARTO,MODIFICA_ASSOCIAZIONE_FIRMATARIO,
		//ATTIVA_FIRMATARIO,DISATTIVA_FIRMATARIO,EDIT_FIRMATARIO,MODIFICA_FIRMATARIO,INSERIMENTO_FIRMATARIO,ELIMINAZIONE_FIRMATARIO,
		//INSERIMENTO_MODELLO_AGID,MODIFICA_MODELLO_AGID,RICERCA_MODELLO_AGID,COPIA_MODELLO_AGID,ELIMINA_MODELLO_AGID
	}
	
	public static enum WS_PAPERLESS_EXHIBITION{
		DIRECT_DOC_STATE_BY_DOCID,DIRECT_DOC_STATE_BY_EXTID,DIRECT_EXHIBITION_BY_DOCID,
		DIRECT_EXHIBITION_BY_EXTID,DOWNLOAD_DOCUMENT_BY_DOCID,DOWNLOAD_DOCUMENT_BY_EXTID,DOWNLOAD_IMPRINTING_BY_DOCID,DOWNLOAD_IMPRINTING_BY_EXTID,DOWNLOAD_MARKED_IMPRINTING_BY_DOCID,
		DOWNLOAD_MARKED_IMPRINTING_BY_EXTID,GET_COMPANY_INFO,MULTIPLE_SEARCH
	}
	public static enum WS_DATACERTA{
		GET_PUBLIC_KEY, GET_HISTORY, GET_ARCHIVE, UPLOAD_DOCUMENT
	}
	public static enum WS_SEARCH_AND_DOWNLOAD{
		SEARCH_DOCUMENT, DOWNLOAD_DOCUMENT
	}
	public static enum APP{
		WEB_SDC,WS_PAPERLESS_EXHIBITION,WS_DATACERTA,WS_SEARCH_AND_DOWNLOAD
	}
	
	private static final long serialVersionUID = 1L;
	
	private static final String APPLICAZIONE="CREA_CLASSI_MASSIVA_SDC";
	
	private String ipSource;
	private Integer id;
	private String username;
	private String azione;
	private String codOggetto;
	private String tipoOggetto;
	private String descrizione;
	private boolean esito;
	private Date dataRichiesta;
	private String applicazione;

	public AuditBean(){
		this.applicazione=APPLICAZIONE;
	}
	
	public AuditBean(Integer id, String ipSource, String username, String azione, String codOggetto, String tipoOggetto,
			String descrizione, boolean esito, Date dataRichiesta) {
		super();
		this.id = id;
		this.ipSource = ipSource;
		this.username = username;
		this.azione = azione;
		this.codOggetto = codOggetto;
		this.tipoOggetto = tipoOggetto;
		this.descrizione = descrizione;
		this.esito = esito;
		this.dataRichiesta = dataRichiesta;
		this.applicazione=APPLICAZIONE;
	}
	
	@Override
	public String toString() {
		return "AuditBean [ipSource=" + ipSource + ", id=" + id + ", username=" + username + ", azione=" + azione
				+ ", codOggetto=" + codOggetto + ", tipoOggetto=" + tipoOggetto + ", descrizione=" + descrizione
				+ ", esito=" + esito + ", dataRichiesta=" + dataRichiesta + ", applicazione=" + applicazione + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIpSource() {
		return ipSource;
	}

	public void setIpSource(String ipSource) {
		this.ipSource = ipSource;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAzione() {
		return azione;
	}

	public void setAzione(String azione) {
		this.azione = azione;
	}

	public String getCodOggetto() {
		return codOggetto;
	}

	public void setCodOggetto(String codOggetto) {
		this.codOggetto = codOggetto;
	}

	public String getTipoOggetto() {
		return tipoOggetto;
	}

	public void setTipoOggetto(String tipoOggetto) {
		this.tipoOggetto = tipoOggetto;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public boolean isEsito() {
		return esito;
	}

	public void setEsito(boolean esito) {
		this.esito = esito;
	}

	public Date getDataRichiesta() {
		return dataRichiesta;
	}

	public void setDataRichiesta(Date dataRichiesta) {
		this.dataRichiesta = dataRichiesta;
	}

	public String getApplicazione() {
		return applicazione;
	}

	public void setApplicazione(String applicazione) {
		this.applicazione = applicazione;
	}
	
}
