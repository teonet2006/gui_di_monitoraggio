package it.postel.bean;

import java.io.Serializable;

public class ReportPddClasseDocBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Integer idReportPdd;
	protected Integer rifClasseDoc;
	protected String pathPdd;
	protected short tipoInvioPdd;
	protected String ipServerFtp;
	protected String portServerFtp;
	protected String userNameFtp;
	protected String passwordFtp;
	protected Boolean flagInvioPdd;
	
	
	@Override
	public String toString() {
		return "ReportPddClasseDocBean [idReportPdd=" + idReportPdd + ", rifClasseDoc=" + rifClasseDoc + ", pathPdd="
				+ pathPdd + ", tipoInvioPdd=" + tipoInvioPdd + ", ipServerFtp=" + ipServerFtp + ", portServerFtp="
				+ portServerFtp + ", userNameFtp=" + userNameFtp + ", passwordFtp=" + passwordFtp + ", flagInvioPdd="
				+ flagInvioPdd + "]";
	}
	public Integer getIdReportPdd() {
		return idReportPdd;
	}
	public void setIdReportPdd(Integer idReportPdd) {
		this.idReportPdd = idReportPdd;
	}
	public Integer getRifClasseDoc() {
		return rifClasseDoc;
	}
	public void setRifClasseDoc(Integer rifClasseDoc) {
		this.rifClasseDoc = rifClasseDoc;
	}
	public String getPathPdd() {
		return pathPdd;
	}
	public void setPathPdd(String pathPdd) {
		this.pathPdd = pathPdd;
	}
	public short getTipoInvioPdd() {
		return tipoInvioPdd;
	}
	public void setTipoInvioPdd(short tipoInvioPdd) {
		this.tipoInvioPdd = tipoInvioPdd;
	}
	public String getIpServerFtp() {
		return ipServerFtp;
	}
	public void setIpServerFtp(String ipServerFtp) {
		this.ipServerFtp = ipServerFtp;
	}
	public String getPortServerFtp() {
		return portServerFtp;
	}
	public void setPortServerFtp(String portServerFtp) {
		this.portServerFtp = portServerFtp;
	}
	public String getUserNameFtp() {
		return userNameFtp;
	}
	public void setUserNameFtp(String userNameFtp) {
		this.userNameFtp = userNameFtp;
	}
	public String getPasswordFtp() {
		return passwordFtp;
	}
	public void setPasswordFtp(String passwordFtp) {
		this.passwordFtp = passwordFtp;
	}
	public boolean configFtpValid() {
		if(this.ipServerFtp!=null&&this.portServerFtp!=null&&this.userNameFtp!=null&&this.passwordFtp!=null) {
			return true;
		} else {
			return false;
		}
	}
	public Boolean getFlagInvioPdd() {
		return flagInvioPdd;
	}
	public void setFlagInvioPdd(Boolean flagInvioPdd) {
		this.flagInvioPdd = flagInvioPdd;
	}
	
}