package it.postel.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ClasseDocumentaleBean implements Serializable{
	/**
	 * 
	 */
	public static enum CLASSEDOC_AZIONE {CREA/*COPIA,MODIFICA,VISUALIZZA*/}
	public static enum TABLE_PARTITION {STORAGE_PDV,STORAGE_PDV_CONSERVATO,STORAGE_PDV_ERRORE
											,STORAGE_DOCUMENTO,STORAGE_DOCUMENTO_CONSERVATO,STORAGE_DOCUMENTO_ERRORE, STORAGE_PDA,TRACKING_GED}
	
	private static final long serialVersionUID = 1L;
	
	private Integer idClasseDoc;
	private String idClasseDocObs;
	private String descrizione;
	private String zCode;
	private boolean flagAttiva;
	private Integer maxDocumentiPda;
	private Integer maxMbPda;	
	private String estensioneDocumenti;
	private boolean externalHashVerifica;
	private String nomeClasseDocumentale;
	private String respConservazione;
	private String riferimento;
	private boolean flagEmissione;
	private Integer giorniChiusuraPda;
	private Integer rifSezionale;	
	private String pathEmissione;	
	private boolean flagArchiviazione;	 	
	private boolean flagConservazione;
	private Integer livelloPriorita;
	private Integer rifTipoValidazione;
	private Integer rifTipologia;
	private boolean flagDismessa;
	private String pathIn;
	private String pathBackupPdv;
	private String pathRdvRdaFtp;	
	private Integer frequenzaAcquisizione;
	private Date lastRunAcquisizione;
	private Integer rifClassDato;
	private Integer rifTermineConservazione;
	private Integer rifFirmatario;
	private Integer rifPeriodoRetention;
	private boolean ctrlUnivocita;
	private boolean flagCdc;
	private boolean flagSezionale;
	private String pathArchiviazione;
	private boolean flagClienteHub;
	private boolean ctrlSequenzialita;
	private boolean flagCifrato;
	private String ragioneSociale;
	private ReportClientiClasseDocBean reportClasseDoc;
	private ReportClientiFtpBean reportFtp;
	private Integer rifWorkflow;
	private Integer rifCodiceAccounting;
	private Integer rifProcedura;
	private Integer rifFirmatarioRespCons;
	private List<MetadatiClasseDocBean> listaMetadati;
	private String tipoClasseDoc;
	private ReportPddClasseDocBean reportPdd;
	private Integer giorniBackupPdv;
	private boolean flagDataCerta;
	private boolean flagTimestamp;
	private boolean flagTrackingGed;
	private Integer meseRiferimento;
	private boolean flagBollato;
	private boolean flagSmistamento;
	private Integer rifTipologiaFirma;
	private String pathPrescarto;
	private String pathScarto;
	private boolean flagSdcId;
	private boolean flagControllaFirmaPdf;
	private boolean flagNotificaOrchestratore;
	private Integer rifModelloAgid;
	public ClasseDocumentaleBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ClasseDocumentaleBean(Integer idClasseDoc, String idClasseDocObs, String descrizione, String zCode,
			boolean flagAttiva, Integer maxDocumentiPda, Integer maxMbPda, String estensioneDocumenti,
			boolean externalHashVerifica, String nomeClasseDocumentale, String respConservazione, String riferimento,
			boolean flagEmissione, Integer giorniChiusuraPda, Integer rifSezionale, String pathEmissione,
			boolean flagArchiviazione, boolean flagConservazione, Integer livelloPriorita, Integer rifTipoValidazione,
			Integer rifTipologia, boolean flagDismessa, String pathIn, String pathBackupPdv, String pathRdvRdaFtp,
			Integer frequenzaAcquisizione, Date lastRunAcquisizione, Integer rifClassDato,
			Integer rifTermineConservazione, Integer rifFirmatario, Integer rifPeriodoRetention, boolean ctrlUnivocita,
			boolean flagCdc, boolean flagSezionale, String pathArchiviazione, boolean flagClienteHub,
			boolean ctrlSequenzialita, boolean flagCifrato, String ragioneSociale,
			ReportClientiClasseDocBean reportClasseDoc, ReportClientiFtpBean reportFtp, Integer rifWorkflow,
			Integer rifCodiceAccounting, Integer rifProcedura, Integer rifFirmatarioRespCons,
			List<MetadatiClasseDocBean> listaMetadati, String tipoClasseDoc, ReportPddClasseDocBean reportPdd,
			Integer giorniBackupPdv, boolean flagDataCerta, boolean flagTimestamp, boolean flagTrackingGed,
			Integer meseRiferimento, boolean flagBollato, boolean flagSmistamento, Integer rifTipologiaFirma,
			String pathPrescarto, String pathScarto, boolean flagSdcId, boolean flagControllaFirmaPdf,
			boolean flagNotificaOrchestratore, Integer rifModelloAgid) {
		super();
		this.idClasseDoc = idClasseDoc;
		this.idClasseDocObs = idClasseDocObs;
		this.descrizione = descrizione;
		this.zCode = zCode;
		this.flagAttiva = flagAttiva;
		this.maxDocumentiPda = maxDocumentiPda;
		this.maxMbPda = maxMbPda;
		this.estensioneDocumenti = estensioneDocumenti;
		this.externalHashVerifica = externalHashVerifica;
		this.nomeClasseDocumentale = nomeClasseDocumentale;
		this.respConservazione = respConservazione;
		this.riferimento = riferimento;
		this.flagEmissione = flagEmissione;
		this.giorniChiusuraPda = giorniChiusuraPda;
		this.rifSezionale = rifSezionale;
		this.pathEmissione = pathEmissione;
		this.flagArchiviazione = flagArchiviazione;
		this.flagConservazione = flagConservazione;
		this.livelloPriorita = livelloPriorita;
		this.rifTipoValidazione = rifTipoValidazione;
		this.rifTipologia = rifTipologia;
		this.flagDismessa = flagDismessa;
		this.pathIn = pathIn;
		this.pathBackupPdv = pathBackupPdv;
		this.pathRdvRdaFtp = pathRdvRdaFtp;
		this.frequenzaAcquisizione = frequenzaAcquisizione;
		this.lastRunAcquisizione = lastRunAcquisizione;
		this.rifClassDato = rifClassDato;
		this.rifTermineConservazione = rifTermineConservazione;
		this.rifFirmatario = rifFirmatario;
		this.rifPeriodoRetention = rifPeriodoRetention;
		this.ctrlUnivocita = ctrlUnivocita;
		this.flagCdc = flagCdc;
		this.flagSezionale = flagSezionale;
		this.pathArchiviazione = pathArchiviazione;
		this.flagClienteHub = flagClienteHub;
		this.ctrlSequenzialita = ctrlSequenzialita;
		this.flagCifrato = flagCifrato;
		this.ragioneSociale = ragioneSociale;
		this.reportClasseDoc = reportClasseDoc;
		this.reportFtp = reportFtp;
		this.rifWorkflow = rifWorkflow;
		this.rifCodiceAccounting = rifCodiceAccounting;
		this.rifProcedura = rifProcedura;
		this.rifFirmatarioRespCons = rifFirmatarioRespCons;
		this.listaMetadati = listaMetadati;
		this.tipoClasseDoc = tipoClasseDoc;
		this.reportPdd = reportPdd;
		this.giorniBackupPdv = giorniBackupPdv;
		this.flagDataCerta = flagDataCerta;
		this.flagTimestamp = flagTimestamp;
		this.flagTrackingGed = flagTrackingGed;
		this.meseRiferimento = meseRiferimento;
		this.flagBollato = flagBollato;
		this.flagSmistamento = flagSmistamento;
		this.rifTipologiaFirma = rifTipologiaFirma;
		this.pathPrescarto = pathPrescarto;
		this.pathScarto = pathScarto;
		this.flagSdcId = flagSdcId;
		this.flagControllaFirmaPdf = flagControllaFirmaPdf;
		this.flagNotificaOrchestratore = flagNotificaOrchestratore;
		this.rifModelloAgid = rifModelloAgid;
	}
	@Override
	public String toString() {
		return "ClasseDocumentaleBean [idClasseDoc=" + idClasseDoc + ", idClasseDocObs=" + idClasseDocObs
				+ ", descrizione=" + descrizione + ", zCode=" + zCode + ", flagAttiva=" + flagAttiva
				+ ", maxDocumentiPda=" + maxDocumentiPda + ", maxMbPda=" + maxMbPda + ", estensioneDocumenti="
				+ estensioneDocumenti + ", externalHashVerifica=" + externalHashVerifica + ", nomeClasseDocumentale="
				+ nomeClasseDocumentale + ", respConservazione=" + respConservazione + ", riferimento=" + riferimento
				+ ", flagEmissione=" + flagEmissione + ", giorniChiusuraPda=" + giorniChiusuraPda + ", rifSezionale="
				+ rifSezionale + ", pathEmissione=" + pathEmissione + ", flagArchiviazione=" + flagArchiviazione
				+ ", flagConservazione=" + flagConservazione + ", livelloPriorita=" + livelloPriorita
				+ ", rifTipoValidazione=" + rifTipoValidazione + ", rifTipologia=" + rifTipologia + ", flagDismessa="
				+ flagDismessa + ", pathIn=" + pathIn + ", pathBackupPdv=" + pathBackupPdv + ", pathRdvRdaFtp="
				+ pathRdvRdaFtp + ", frequenzaAcquisizione=" + frequenzaAcquisizione + ", lastRunAcquisizione="
				+ lastRunAcquisizione + ", rifClassDato=" + rifClassDato + ", rifTermineConservazione="
				+ rifTermineConservazione + ", rifFirmatario=" + rifFirmatario + ", rifPeriodoRetention="
				+ rifPeriodoRetention + ", ctrlUnivocita=" + ctrlUnivocita + ", flagCdc=" + flagCdc + ", flagSezionale="
				+ flagSezionale + ", pathArchiviazione=" + pathArchiviazione + ", flagClienteHub=" + flagClienteHub
				+ ", ctrlSequenzialita=" + ctrlSequenzialita + ", flagCifrato=" + flagCifrato + ", ragioneSociale="
				+ ragioneSociale + ", reportClasseDoc=" + reportClasseDoc + ", reportFtp=" + reportFtp
				+ ", rifWorkflow=" + rifWorkflow + ", rifCodiceAccounting=" + rifCodiceAccounting + ", rifProcedura="
				+ rifProcedura + ", rifFirmatarioRespCons=" + rifFirmatarioRespCons + ", listaMetadati=" + listaMetadati
				+ ", tipoClasseDoc=" + tipoClasseDoc + ", reportPdd=" + reportPdd + ", giorniBackupPdv="
				+ giorniBackupPdv + ", flagDataCerta=" + flagDataCerta + ", flagTimestamp=" + flagTimestamp
				+ ", flagTrackingGed=" + flagTrackingGed + ", meseRiferimento=" + meseRiferimento + ", flagBollato="
				+ flagBollato + ", flagSmistamento=" + flagSmistamento + ", rifTipologiaFirma=" + rifTipologiaFirma
				+ ", pathPrescarto=" + pathPrescarto + ", pathScarto=" + pathScarto + ", flagSdcId=" + flagSdcId
				+ ", flagControllaFirmaPdf=" + flagControllaFirmaPdf + ", flagNotificaOrchestratore="
				+ flagNotificaOrchestratore + ", rifModelloAgid=" + rifModelloAgid + "]";
	}
	public Integer getIdClasseDoc() {
		return idClasseDoc;
	}
	public void setIdClasseDoc(Integer idClasseDoc) {
		this.idClasseDoc = idClasseDoc;
	}
	public String getIdClasseDocObs() {
		return idClasseDocObs;
	}
	public void setIdClasseDocObs(String idClasseDocObs) {
		this.idClasseDocObs = idClasseDocObs;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getzCode() {
		return zCode;
	}
	public void setzCode(String zCode) {
		this.zCode = zCode;
	}
	public boolean isFlagAttiva() {
		return flagAttiva;
	}
	public void setFlagAttiva(boolean flagAttiva) {
		this.flagAttiva = flagAttiva;
	}
	public Integer getMaxDocumentiPda() {
		return maxDocumentiPda;
	}
	public void setMaxDocumentiPda(Integer maxDocumentiPda) {
		this.maxDocumentiPda = maxDocumentiPda;
	}
	public Integer getMaxMbPda() {
		return maxMbPda;
	}
	public void setMaxMbPda(Integer maxMbPda) {
		this.maxMbPda = maxMbPda;
	}
	public String getEstensioneDocumenti() {
		return estensioneDocumenti;
	}
	public void setEstensioneDocumenti(String estensioneDocumenti) {
		this.estensioneDocumenti = estensioneDocumenti;
	}
	public boolean isExternalHashVerifica() {
		return externalHashVerifica;
	}
	public void setExternalHashVerifica(boolean externalHashVerifica) {
		this.externalHashVerifica = externalHashVerifica;
	}
	public String getNomeClasseDocumentale() {
		return nomeClasseDocumentale;
	}
	public void setNomeClasseDocumentale(String nomeClasseDocumentale) {
		this.nomeClasseDocumentale = nomeClasseDocumentale;
	}
	public String getRespConservazione() {
		return respConservazione;
	}
	public void setRespConservazione(String respConservazione) {
		this.respConservazione = respConservazione;
	}
	public String getRiferimento() {
		return riferimento;
	}
	public void setRiferimento(String riferimento) {
		this.riferimento = riferimento;
	}
	public boolean isFlagEmissione() {
		return flagEmissione;
	}
	public void setFlagEmissione(boolean flagEmissione) {
		this.flagEmissione = flagEmissione;
	}
	public Integer getGiorniChiusuraPda() {
		return giorniChiusuraPda;
	}
	public void setGiorniChiusuraPda(Integer giorniChiusuraPda) {
		this.giorniChiusuraPda = giorniChiusuraPda;
	}
	public Integer getRifSezionale() {
		return rifSezionale;
	}
	public void setRifSezionale(Integer rifSezionale) {
		this.rifSezionale = rifSezionale;
	}
	public String getPathEmissione() {
		return pathEmissione;
	}
	public void setPathEmissione(String pathEmissione) {
		this.pathEmissione = pathEmissione;
	}
	public boolean isFlagArchiviazione() {
		return flagArchiviazione;
	}
	public void setFlagArchiviazione(boolean flagArchiviazione) {
		this.flagArchiviazione = flagArchiviazione;
	}
	public boolean isFlagConservazione() {
		return flagConservazione;
	}
	public void setFlagConservazione(boolean flagConservazione) {
		this.flagConservazione = flagConservazione;
	}
	public Integer getLivelloPriorita() {
		return livelloPriorita;
	}
	public void setLivelloPriorita(Integer livelloPriorita) {
		this.livelloPriorita = livelloPriorita;
	}
	public Integer getRifTipoValidazione() {
		return rifTipoValidazione;
	}
	public void setRifTipoValidazione(Integer rifTipoValidazione) {
		this.rifTipoValidazione = rifTipoValidazione;
	}
	public Integer getRifTipologia() {
		return rifTipologia;
	}
	public void setRifTipologia(Integer rifTipologia) {
		this.rifTipologia = rifTipologia;
	}
	public boolean isFlagDismessa() {
		return flagDismessa;
	}
	public void setFlagDismessa(boolean flagDismessa) {
		this.flagDismessa = flagDismessa;
	}
	public String getPathIn() {
		return pathIn;
	}
	public void setPathIn(String pathIn) {
		this.pathIn = pathIn;
	}
	public String getPathBackupPdv() {
		return pathBackupPdv;
	}
	public void setPathBackupPdv(String pathBackupPdv) {
		this.pathBackupPdv = pathBackupPdv;
	}
	public String getPathRdvRdaFtp() {
		return pathRdvRdaFtp;
	}
	public void setPathRdvRdaFtp(String pathRdvRdaFtp) {
		this.pathRdvRdaFtp = pathRdvRdaFtp;
	}
	public Integer getFrequenzaAcquisizione() {
		return frequenzaAcquisizione;
	}
	public void setFrequenzaAcquisizione(Integer frequenzaAcquisizione) {
		this.frequenzaAcquisizione = frequenzaAcquisizione;
	}
	public Date getLastRunAcquisizione() {
		return lastRunAcquisizione;
	}
	public void setLastRunAcquisizione(Date lastRunAcquisizione) {
		this.lastRunAcquisizione = lastRunAcquisizione;
	}
	public Integer getRifClassDato() {
		return rifClassDato;
	}
	public void setRifClassDato(Integer rifClassDato) {
		this.rifClassDato = rifClassDato;
	}
	public Integer getRifTermineConservazione() {
		return rifTermineConservazione;
	}
	public void setRifTermineConservazione(Integer rifTermineConservazione) {
		this.rifTermineConservazione = rifTermineConservazione;
	}
	public Integer getRifFirmatario() {
		return rifFirmatario;
	}
	public void setRifFirmatario(Integer rifFirmatario) {
		this.rifFirmatario = rifFirmatario;
	}
	public Integer getRifPeriodoRetention() {
		return rifPeriodoRetention;
	}
	public void setRifPeriodoRetention(Integer rifPeriodoRetention) {
		this.rifPeriodoRetention = rifPeriodoRetention;
	}
	public boolean isCtrlUnivocita() {
		return ctrlUnivocita;
	}
	public void setCtrlUnivocita(boolean ctrlUnivocita) {
		this.ctrlUnivocita = ctrlUnivocita;
	}
	public boolean isFlagCdc() {
		return flagCdc;
	}
	public void setFlagCdc(boolean flagCdc) {
		this.flagCdc = flagCdc;
	}
	public boolean isFlagSezionale() {
		return flagSezionale;
	}
	public void setFlagSezionale(boolean flagSezionale) {
		this.flagSezionale = flagSezionale;
	}
	public String getPathArchiviazione() {
		return pathArchiviazione;
	}
	public void setPathArchiviazione(String pathArchiviazione) {
		this.pathArchiviazione = pathArchiviazione;
	}
	public boolean isFlagClienteHub() {
		return flagClienteHub;
	}
	public void setFlagClienteHub(boolean flagClienteHub) {
		this.flagClienteHub = flagClienteHub;
	}
	public boolean isCtrlSequenzialita() {
		return ctrlSequenzialita;
	}
	public void setCtrlSequenzialita(boolean ctrlSequenzialita) {
		this.ctrlSequenzialita = ctrlSequenzialita;
	}
	public boolean isFlagCifrato() {
		return flagCifrato;
	}
	public void setFlagCifrato(boolean flagCifrato) {
		this.flagCifrato = flagCifrato;
	}
	public String getRagioneSociale() {
		return ragioneSociale;
	}
	public void setRagioneSociale(String ragioneSociale) {
		this.ragioneSociale = ragioneSociale;
	}
	public ReportClientiClasseDocBean getReportClasseDoc() {
		return reportClasseDoc;
	}
	public void setReportClasseDoc(ReportClientiClasseDocBean reportClasseDoc) {
		this.reportClasseDoc = reportClasseDoc;
	}
	public ReportClientiFtpBean getReportFtp() {
		return reportFtp;
	}
	public void setReportFtp(ReportClientiFtpBean reportFtp) {
		this.reportFtp = reportFtp;
	}
	public Integer getRifWorkflow() {
		return rifWorkflow;
	}
	public void setRifWorkflow(Integer rifWorkflow) {
		this.rifWorkflow = rifWorkflow;
	}
	public Integer getRifCodiceAccounting() {
		return rifCodiceAccounting;
	}
	public void setRifCodiceAccounting(Integer rifCodiceAccounting) {
		this.rifCodiceAccounting = rifCodiceAccounting;
	}
	public Integer getRifProcedura() {
		return rifProcedura;
	}
	public void setRifProcedura(Integer rifProcedura) {
		this.rifProcedura = rifProcedura;
	}
	public Integer getRifFirmatarioRespCons() {
		return rifFirmatarioRespCons;
	}
	public void setRifFirmatarioRespCons(Integer rifFirmatarioRespCons) {
		this.rifFirmatarioRespCons = rifFirmatarioRespCons;
	}
	public List<MetadatiClasseDocBean> getListaMetadati() {
		return listaMetadati;
	}
	public void setListaMetadati(List<MetadatiClasseDocBean> listaMetadati) {
		this.listaMetadati = listaMetadati;
	}
	public String getTipoClasseDoc() {
		return tipoClasseDoc;
	}
	public void setTipoClasseDoc(String tipoClasseDoc) {
		this.tipoClasseDoc = tipoClasseDoc;
	}
	public ReportPddClasseDocBean getReportPdd() {
		return reportPdd;
	}
	public void setReportPdd(ReportPddClasseDocBean reportPdd) {
		this.reportPdd = reportPdd;
	}
	public Integer getGiorniBackupPdv() {
		return giorniBackupPdv;
	}
	public void setGiorniBackupPdv(Integer giorniBackupPdv) {
		this.giorniBackupPdv = giorniBackupPdv;
	}
	public boolean isFlagDataCerta() {
		return flagDataCerta;
	}
	public void setFlagDataCerta(boolean flagDataCerta) {
		this.flagDataCerta = flagDataCerta;
	}
	public boolean isFlagTimestamp() {
		return flagTimestamp;
	}
	public void setFlagTimestamp(boolean flagTimestamp) {
		this.flagTimestamp = flagTimestamp;
	}
	public boolean isFlagTrackingGed() {
		return flagTrackingGed;
	}
	public void setFlagTrackingGed(boolean flagTrackingGed) {
		this.flagTrackingGed = flagTrackingGed;
	}
	public Integer getMeseRiferimento() {
		return meseRiferimento;
	}
	public void setMeseRiferimento(Integer meseRiferimento) {
		this.meseRiferimento = meseRiferimento;
	}
	public boolean isFlagBollato() {
		return flagBollato;
	}
	public void setFlagBollato(boolean flagBollato) {
		this.flagBollato = flagBollato;
	}
	public boolean isFlagSmistamento() {
		return flagSmistamento;
	}
	public void setFlagSmistamento(boolean flagSmistamento) {
		this.flagSmistamento = flagSmistamento;
	}
	public Integer getRifTipologiaFirma() {
		return rifTipologiaFirma;
	}
	public void setRifTipologiaFirma(Integer rifTipologiaFirma) {
		this.rifTipologiaFirma = rifTipologiaFirma;
	}
	public String getPathPrescarto() {
		return pathPrescarto;
	}
	public void setPathPrescarto(String pathPrescarto) {
		this.pathPrescarto = pathPrescarto;
	}
	public String getPathScarto() {
		return pathScarto;
	}
	public void setPathScarto(String pathScarto) {
		this.pathScarto = pathScarto;
	}
	public boolean isFlagSdcId() {
		return flagSdcId;
	}
	public void setFlagSdcId(boolean flagSdcId) {
		this.flagSdcId = flagSdcId;
	}
	public boolean isFlagControllaFirmaPdf() {
		return flagControllaFirmaPdf;
	}
	public void setFlagControllaFirmaPdf(boolean flagControllaFirmaPdf) {
		this.flagControllaFirmaPdf = flagControllaFirmaPdf;
	}
	public boolean isFlagNotificaOrchestratore() {
		return flagNotificaOrchestratore;
	}
	public void setFlagNotificaOrchestratore(boolean flagNotificaOrchestratore) {
		this.flagNotificaOrchestratore = flagNotificaOrchestratore;
	}
	public Integer getRifModelloAgid() {
		return rifModelloAgid;
	}
	public void setRifModelloAgid(Integer rifModelloAgid) {
		this.rifModelloAgid = rifModelloAgid;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}





}
