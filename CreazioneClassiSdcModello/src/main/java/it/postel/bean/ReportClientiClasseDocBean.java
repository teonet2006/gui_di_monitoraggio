package it.postel.bean;

import java.io.Serializable;

public class ReportClientiClasseDocBean  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Integer idReportClienti;//ID_REPORT_CLIENTI
	protected long rifClasseDoc;//RIF_CLASSE_DOC
	protected short indiciRapportoConservazione; //INDICI_RAPPORTO_CONSERVAZIONE
	protected short flagInvioRdv;	//FLAG_INVIO_RDV
	protected String pathRapportoVersamento;//PATH_RAPPORTO_VERSAMENTO
	protected short tipoInvioRdv;//TIPO_INVIO_RDV
	protected String pathRapportoArchiviazione;//PATH_RAPPORTO_ARCHIVIAZIONE
	protected short tipoInvioRda;//TIPO_INVIO_RDA
	protected short flagInvioRda;//FLAG_INVIO_RDA
	protected short tipoIdentificativoEsterno;//TIPO_IDENTIFICATIVO_ESTERNO

	
	
	
	
	@Override
	public String toString() {
		return "ReportClientiClasseDocBean [idReportClienti=" + idReportClienti + ", rifClasseDoc=" + rifClasseDoc
				+ ", indiciRapportoConservazione=" + indiciRapportoConservazione + ", flagInvioRdv=" + flagInvioRdv
				+ ", pathRapportoVersamento=" + pathRapportoVersamento + ", tipoInvioRdv=" + tipoInvioRdv
				+ ", pathRapportoArchiviazione=" + pathRapportoArchiviazione + ", tipoInvioRda=" + tipoInvioRda
				+ ", flagInvioRda=" + flagInvioRda + ", tipoIdentificativoEsterno=" + tipoIdentificativoEsterno + "]";
	}
	public Integer getIdReportClienti() {
		return idReportClienti;
	}
	public void setIdReportClienti(Integer idReportClienti) {
		this.idReportClienti = idReportClienti;
	}
	public long getRifClasseDoc() {
		return rifClasseDoc;
	}
	public void setRifClasseDoc(long rifClasseDoc) {
		this.rifClasseDoc = rifClasseDoc;
	}
	public short getIndiciRapportoConservazione() {
		return indiciRapportoConservazione;
	}
	public void setIndiciRapportoConservazione(short indiciRapportoConservazione) {
		this.indiciRapportoConservazione = indiciRapportoConservazione;
	}
	public short getFlagInvioRdv() {
		return flagInvioRdv;
	}
	public void setFlagInvioRdv(short flagInvioRdv) {
		this.flagInvioRdv = flagInvioRdv;
	}
	public String getPathRapportoVersamento() {
		return pathRapportoVersamento;
	}
	public void setPathRapportoVersamento(String pathRapportoVersamento) {
		this.pathRapportoVersamento = pathRapportoVersamento;
	}
	public short getTipoInvioRdv() {
		return tipoInvioRdv;
	}
	public void setTipoInvioRdv(short tipoInvioRdv) {
		this.tipoInvioRdv = tipoInvioRdv;
	}
	public String getPathRapportoArchiviazione() {
		return pathRapportoArchiviazione;
	}
	public void setPathRapportoArchiviazione(String pathRapportoArchiviazione) {
		this.pathRapportoArchiviazione = pathRapportoArchiviazione;
	}
	public short getTipoInvioRda() {
		return tipoInvioRda;
	}
	public void setTipoInvioRda(short tipoInvioRda) {
		this.tipoInvioRda = tipoInvioRda;
	}
	public short getFlagInvioRda() {
		return flagInvioRda;
	}
	public void setFlagInvioRda(short flagInvioRda) {
		this.flagInvioRda = flagInvioRda;
	}
	public short getTipoIdentificativoEsterno() {
		return tipoIdentificativoEsterno;
	}
	public void setTipoIdentificativoEsterno(short tipoIdentificativoEsterno) {
		this.tipoIdentificativoEsterno = tipoIdentificativoEsterno;
	}
	
}
