package it.postel.bean;

public class CodBean {
	//private Long newId;
	private int lavorazioneId;
	private String nomeRepo;
	private String midaCodeField;
	private String numPagine;
	private String campoStatoSostitut;
	private String statoSostitut;
	private String statoSostitutInviatoCons;
	private String statoSostitutErroreCons;
	private String maxCpx;
	private String repoUsername;
	private String repoPwd;
	private String dfcProp;
	private String classiDocMap;
	private String nomeClasseDctm;
	
	
	//public Long getNewId() {
	//	return newId;
	//}
	//public void setNewId(Long newId) {
	//	this.newId = newId;
	//}
	
	public int getLavorazioneId() {
		return lavorazioneId;
	}
	public void setLavorazioneId(int lavorazioneId) {
		this.lavorazioneId = lavorazioneId;
	}
	public String getNomeRepo() {
		return nomeRepo;
	}
	public void setNomeRepo(String nomeRepo) {
		this.nomeRepo = nomeRepo;
	}
	public String getMidaCodeField() {
		return midaCodeField;
	}
	public void setMidaCodeField(String midaCodeField) {
		this.midaCodeField = midaCodeField;
	}
	public String getNumPagine() {
		return numPagine;
	}
	public void setNumPagine(String numPagine) {
		this.numPagine = numPagine;
	}
	public String getCampoStatoSostitut() {
		return campoStatoSostitut;
	}
	public void setCampoStatoSostitut(String campoStatoSostitut) {
		this.campoStatoSostitut = campoStatoSostitut;
	}
	public String getStatoSostitut() {
		return statoSostitut;
	}
	public void setStatoSostitut(String statoSostitut) {
		this.statoSostitut = statoSostitut;
	}
	public String getStatoSostitutInviatoCons() {
		return statoSostitutInviatoCons;
	}
	public void setStatoSostitutInviatoCons(String statoSostitutInviatoCons) {
		this.statoSostitutInviatoCons = statoSostitutInviatoCons;
	}
	public String getStatoSostitutErroreCons() {
		return statoSostitutErroreCons;
	}
	public void setStatoSostitutErroreCons(String statoSostitutErroreCons) {
		this.statoSostitutErroreCons = statoSostitutErroreCons;
	}
	public String getMaxCpx() {
		return maxCpx;
	}
	public void setMaxCpx(String maxCpx) {
		this.maxCpx = maxCpx;
	}
	public String getRepoUsername() {
		return repoUsername;
	}
	public void setRepoUsername(String repoUsername) {
		this.repoUsername = repoUsername;
	}
	public String getRepoPwd() {
		return repoPwd;
	}
	public void setRepoPwd(String repoPwd) {
		this.repoPwd = repoPwd;
	}
	public String getDfcProp() {
		return dfcProp;
	}
	public void setDfcProp(String dfcProp) {
		this.dfcProp = dfcProp;
	}
	public String getClassiDocMap() {
		return classiDocMap;
	}
	public void setClassiDocMap(String classiDocMap) {
		this.classiDocMap = classiDocMap;
	}
	public String getNomeClasseDctm() {
		return nomeClasseDctm;
	}
	public void setNomeClasseDctm(String nomeClasseDctm) {
		this.nomeClasseDctm = nomeClasseDctm;
	}
	@Override
	public String toString() {
		return "CodBean [lavorazioneId=" + lavorazioneId + ", nomeRepo=" + nomeRepo + ", midaCodeField="
				+ midaCodeField + ", numPagine=" + numPagine + ", campoStatoSostitut=" + campoStatoSostitut
				+ ", statoSostitut=" + statoSostitut + ", statoSostitutInviatoCons=" + statoSostitutInviatoCons
				+ ", statoSostitutErroreCons=" + statoSostitutErroreCons + ", maxCpx=" + maxCpx + ", repoUsername="
				+ repoUsername + ", repoPwd=" + repoPwd + ", dfcProp=" + dfcProp + ", classiDocMap=" + classiDocMap
				+ ", nomeClasseDctm=" + nomeClasseDctm + "]";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
