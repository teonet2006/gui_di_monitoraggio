package it.postel.bean;

//classe di valori modificati per nuova classe documentale 

public class ValoriOut {
	
	
	//STORAGE_CLASSE_DOC
	private Integer idClasseDoc; //da param 
	private String descrizione; //Z00xxxxx_<NOME_PROCEDURA>
	private String zcode; //da param
	private String nomeClasseDocumentale; //Z00xxxxx_<NOME_PROCEDURA>
	private String pathIn; //mnt/sdc_cert_input/Z00xxxxx/inputFolder
	private String pathBackupPdv; //mnt/sdc_cert_input/backupFolder
	private String pathArchiviazione; //mnt/sdc_cert_input/Z0087499/archiviazione-oil/
	private String pathPrescarto; //mnt/sdc_cert_input/Z0087499/bps_pec_outbox_bisse/scarto/
	private String pathScarto; //mnt/sdc_cert_input/Z0087499/bps_pec_outbox_bisse/prescarto/
	
	//STORAGE_REPORT_CLIENTI_CLASSE_DOC
	private String pathRapportoConservazione;
	private String pathRapportoArchiviazione;
	private String pathRapportoVersamento;
	
	
	public ValoriOut() {
		super();
		// TODO Auto-generated constructor stub
	}


	public ValoriOut(int idClasseDoc, String descrizione, String zcode, String nomeClasseDocumentale, String pathIn,
			String pathBackupPdv, String pathArchiviazione, String pathPrescarto, String pathScarto,
			String pathRapportoConservazione, String pathRapportoArchiviazione, String pathRapportoVersamento) {
		super();
		this.idClasseDoc = idClasseDoc;
		this.descrizione = descrizione;
		this.zcode = zcode;
		this.nomeClasseDocumentale = nomeClasseDocumentale;
		this.pathIn = pathIn;
		this.pathBackupPdv = pathBackupPdv;
		this.pathArchiviazione = pathArchiviazione;
		this.pathPrescarto = pathPrescarto;
		this.pathScarto = pathScarto;
		this.pathRapportoConservazione = pathRapportoConservazione;
		this.pathRapportoArchiviazione = pathRapportoArchiviazione;
		this.pathRapportoVersamento = pathRapportoVersamento;
	}


	@Override
	public String toString() {
		return "ValoriOut [idClasseDoc=" + idClasseDoc + ", descrizione=" + descrizione + ", zcode=" + zcode
				+ ", nomeClasseDocumentale=" + nomeClasseDocumentale + ", pathIn=" + pathIn + ", pathBackupPdv="
				+ pathBackupPdv + ", pathArchiviazione=" + pathArchiviazione + ", pathPrescarto=" + pathPrescarto
				+ ", pathScarto=" + pathScarto + ", pathRapportoConservazione=" + pathRapportoConservazione
				+ ", pathRapportoArchiviazione=" + pathRapportoArchiviazione + ", pathRapportoVersamento="
				+ pathRapportoVersamento + "]";
	}


	public int getIdClasseDoc() {
		return idClasseDoc;
	}


	public void setIdClasseDoc(int idClasseDoc) {
		this.idClasseDoc = idClasseDoc;
	}


	public String getDescrizione() {
		return descrizione;
	}


	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}


	public String getZcode() {
		return zcode;
	}


	public void setZcode(String zcode) {
		this.zcode = zcode;
	}


	public String getNomeClasseDocumentale() {
		return nomeClasseDocumentale;
	}


	public void setNomeClasseDocumentale(String nomeClasseDocumentale) {
		this.nomeClasseDocumentale = nomeClasseDocumentale;
	}


	public String getPathIn() {
		return pathIn;
	}


	public void setPathIn(String pathIn) {
		this.pathIn = pathIn;
	}


	public String getPathBackupPdv() {
		return pathBackupPdv;
	}


	public void setPathBackupPdv(String pathBackupPdv) {
		this.pathBackupPdv = pathBackupPdv;
	}


	public String getPathArchiviazione() {
		return pathArchiviazione;
	}


	public void setPathArchiviazione(String pathArchiviazione) {
		this.pathArchiviazione = pathArchiviazione;
	}


	public String getPathPrescarto() {
		return pathPrescarto;
	}


	public void setPathPrescarto(String pathPrescarto) {
		this.pathPrescarto = pathPrescarto;
	}


	public String getPathScarto() {
		return pathScarto;
	}


	public void setPathScarto(String pathScarto) {
		this.pathScarto = pathScarto;
	}


	public String getPathRapportoConservazione() {
		return pathRapportoConservazione;
	}


	public void setPathRapportoConservazione(String pathRapportoConservazione) {
		this.pathRapportoConservazione = pathRapportoConservazione;
	}


	public String getPathRapportoArchiviazione() {
		return pathRapportoArchiviazione;
	}


	public void setPathRapportoArchiviazione(String pathRapportoArchiviazione) {
		this.pathRapportoArchiviazione = pathRapportoArchiviazione;
	}


	public String getPathRapportoVersamento() {
		return pathRapportoVersamento;
	}


	public void setPathRapportoVersamento(String pathRapportoVersamento) {
		this.pathRapportoVersamento = pathRapportoVersamento;
	}
	
	
	
	
	
	

}
