package it.postel.bean;

public class ErroreBean {
	private String pathNew;
	private String codiceErrore;
	private String descrizioneErrore;
	private int flagLavoratoNew;
	
	public ErroreBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ErroreBean(String pathNew, String codiceErrore, String descrizioneErrore, int flagLavoratoNew) {
		super();
		this.pathNew = pathNew;
		this.codiceErrore = codiceErrore;
		this.descrizioneErrore = descrizioneErrore;
		this.flagLavoratoNew = flagLavoratoNew;
	}
	@Override
	public String toString() {
		return "ErroreBean [pathNew=" + pathNew + ", codiceErrore=" + codiceErrore + ", descrizioneErrore="
				+ descrizioneErrore + ", flagLavoratoNew=" + flagLavoratoNew + "]";
	}
	public String getPathNew() {
		return pathNew;
	}
	public void setPathNew(String pathNew) {
		this.pathNew = pathNew;
	}
	public String getCodiceErrore() {
		return codiceErrore;
	}
	public void setCodiceErrore(String codiceErrore) {
		this.codiceErrore = codiceErrore;
	}
	public String getDescrizioneErrore() {
		return descrizioneErrore;
	}
	public void setDescrizioneErrore(String descrizioneErrore) {
		this.descrizioneErrore = descrizioneErrore;
	}
	public int getFlagLavoratoNew() {
		return flagLavoratoNew;
	}
	public void setFlagLavoratoNew(int flagLavoratoNew) {
		this.flagLavoratoNew = flagLavoratoNew;
	}
	

	
	
	
	
}
