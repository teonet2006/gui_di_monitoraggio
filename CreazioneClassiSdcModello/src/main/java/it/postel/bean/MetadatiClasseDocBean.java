package it.postel.bean;

import java.io.Serializable;
import java.util.Date;

public class MetadatiClasseDocBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected String etichetta;
	protected Integer lunghezza;
	protected boolean flagRicerca;
	protected boolean flagUnivocita;
	// campo dalla tabella tipolog_tipo_dati_meta
	protected String tipoDatoMeta;
	protected String nomeColIndex;
	protected boolean flagObbligatorieta;
	protected String sostitutoObbligatorieta;
	protected boolean flagCdc;
	protected boolean flagSezionale;
	protected boolean flagSequenza;
	protected boolean flagDataRiferimento;
	protected Integer idMetadato;
	protected Integer rifClasseDoc;
	private String stringValue;
	private Date dateValue;
	private boolean flagRepeating;
	protected boolean flagAgid;
	protected String valoreDefault;
	protected Integer rifModelloAgid;
	protected boolean flagExternalHash;
	
	
	
	
	@Override
	public String toString() {
		return "MetadatiClasseDocBean [etichetta=" + etichetta + ", lunghezza=" + lunghezza + ", flagRicerca="
				+ flagRicerca + ", flagUnivocita=" + flagUnivocita + ", tipoDatoMeta=" + tipoDatoMeta
				+ ", nomeColIndex=" + nomeColIndex + ", flagObbligatorieta=" + flagObbligatorieta
				+ ", sostitutoObbligatorieta=" + sostitutoObbligatorieta + ", flagCdc=" + flagCdc + ", flagSezionale="
				+ flagSezionale + ", flagSequenza=" + flagSequenza + ", flagDataRiferimento=" + flagDataRiferimento
				+ ", idMetadato=" + idMetadato + ", rifClasseDoc=" + rifClasseDoc + ", stringValue=" + stringValue
				+ ", dateValue=" + dateValue + ", flagRepeating=" + flagRepeating + ", flagAgid=" + flagAgid
				+ ", valoreDefault=" + valoreDefault + ", rifModelloAgid=" + rifModelloAgid + ", flagExternalHash="
				+ flagExternalHash + "]";
	}
	public boolean isFlagExternalHash() {
		return flagExternalHash;
	}
	public void setFlagExternalHash(boolean flagExternalHash) {
		this.flagExternalHash = flagExternalHash;
	}
	public Integer getRifModelloAgid() {
		return rifModelloAgid;
	}
	public void setRifModelloAgid(Integer rifModelloAgid) {
		this.rifModelloAgid = rifModelloAgid;
	}
	public String getValoreDefault() {
		return valoreDefault;
	}
	public void setValoreDefault(String valoreDefault) {
		this.valoreDefault = valoreDefault;
	}
	public boolean isFlagAgid() {
		return flagAgid;
	}
	public void setFlagAgid(boolean flagAgid) {
		this.flagAgid = flagAgid;
	}
	public boolean isFlagRepeating() {
		return flagRepeating;
	}
	public void setFlagRepeating(boolean flagRepeating) {
		this.flagRepeating = flagRepeating;
	}
	public String getEtichetta() {
		return etichetta;
	}
	public void setEtichetta(String etichetta) {
		this.etichetta = etichetta;
	}
	public Integer getLunghezza() {
		return lunghezza;
	}
	public void setLunghezza(Integer lunghezza) {
		this.lunghezza = lunghezza;
	}
	public boolean isFlagRicerca() {
		return flagRicerca;
	}
	public void setFlagRicerca(boolean flagRicerca) {
		this.flagRicerca = flagRicerca;
	}
	public boolean isFlagUnivocita() {
		return flagUnivocita;
	}
	public void setFlagUnivocita(boolean flagUnivocita) {
		this.flagUnivocita = flagUnivocita;
	}
	public String getTipoDatoMeta() {
		return tipoDatoMeta;
	}
	public void setTipoDatoMeta(String tipoDatoMeta) {
		this.tipoDatoMeta = tipoDatoMeta;
	}
	public String getNomeColIndex() {
		return nomeColIndex;
	}
	public void setNomeColIndex(String nomeColIndex) {
		this.nomeColIndex = nomeColIndex;
	}
	public boolean isFlagObbligatorieta() {
		return flagObbligatorieta;
	}
	public void setFlagObbligatorieta(boolean flagObbligatorieta) {
		this.flagObbligatorieta = flagObbligatorieta;
	}
	public String getSostitutoObbligatorieta() {
		return sostitutoObbligatorieta;
	}
	public void setSostitutoObbligatorieta(String sostitutoObbligatorieta) {
		this.sostitutoObbligatorieta = sostitutoObbligatorieta;
	}
	public boolean isFlagCdc() {
		return flagCdc;
	}
	public void setFlagCdc(boolean flagCdc) {
		this.flagCdc = flagCdc;
	}
	public boolean isFlagSezionale() {
		return flagSezionale;
	}
	public void setFlagSezionale(boolean flagSezionale) {
		this.flagSezionale = flagSezionale;
	}
	public boolean isFlagSequenza() {
		return flagSequenza;
	}
	public void setFlagSequenza(boolean flagSequenza) {
		this.flagSequenza = flagSequenza;
	}
	public boolean isFlagDataRiferimento() {
		return flagDataRiferimento;
	}
	public void setFlagDataRiferimento(boolean flagDataRiferimento) {
		this.flagDataRiferimento = flagDataRiferimento;
	}
	public Integer getIdMetadato() {
		return idMetadato;
	}
	public void setIdMetadato(Integer idMetadato) {
		this.idMetadato = idMetadato;
	}
	public Integer getRifClasseDoc() {
		return rifClasseDoc;
	}
	public void setRifClasseDoc(Integer rifClasseDoc) {
		this.rifClasseDoc = rifClasseDoc;
	}
	public String getStringValue() {
		return stringValue;
	}
	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}
	public Date getDateValue() {
		return dateValue;
	}
	public void setDateValue(Date dateValue) {
		this.dateValue = dateValue;
	}
	
}
