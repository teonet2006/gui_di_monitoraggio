//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2021.10.02 alle 02:10:40 PM CEST 
//


package it.postel.bean;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="object_fields">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="object_field" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="source_data">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="source_field" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="source_label" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}byte" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="dest_data">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="dest_field" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="dest_label" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "objectFields"
})
@XmlRootElement(name = "object_mapping")
public class ObjectMapping {

    @XmlElement(name = "object_fields", required = true)
    protected ObjectMapping.ObjectFields objectFields;

    /**
     * Recupera il valore della proprietà objectFields.
     * 
     * @return
     *     possible object is
     *     {@link ObjectMapping.ObjectFields }
     *     
     */
    public ObjectMapping.ObjectFields getObjectFields() {
        return objectFields;
    }

    /**
     * Imposta il valore della proprietà objectFields.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectMapping.ObjectFields }
     *     
     */
    public void setObjectFields(ObjectMapping.ObjectFields value) {
        this.objectFields = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="object_field" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="source_data">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="source_field" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="source_label" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}byte" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="dest_data">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="dest_field" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="dest_label" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "objectField"
    })
    public static class ObjectFields {

        @XmlElement(name = "object_field")
        protected List<ObjectMapping.ObjectFields.ObjectField> objectField;

        /**
         * Gets the value of the objectField property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the objectField property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getObjectField().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ObjectMapping.ObjectFields.ObjectField }
         * 
         * 
         */
        public List<ObjectMapping.ObjectFields.ObjectField> getObjectField() {
            if (objectField == null) {
                objectField = new ArrayList<ObjectMapping.ObjectFields.ObjectField>();
            }
            return this.objectField;
        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="source_data">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="source_field" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="source_label" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}byte" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="dest_data">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="dest_field" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="dest_label" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "sourceData",
            "destData"
        })
        public static class ObjectField {

            @XmlElement(name = "source_data", required = true)
            protected ObjectMapping.ObjectFields.ObjectField.SourceData sourceData;
            @XmlElement(name = "dest_data", required = true)
            protected ObjectMapping.ObjectFields.ObjectField.DestData destData;

            /**
             * Recupera il valore della proprietà sourceData.
             * 
             * @return
             *     possible object is
             *     {@link ObjectMapping.ObjectFields.ObjectField.SourceData }
             *     
             */
            public ObjectMapping.ObjectFields.ObjectField.SourceData getSourceData() {
                return sourceData;
            }

            /**
             * Imposta il valore della proprietà sourceData.
             * 
             * @param value
             *     allowed object is
             *     {@link ObjectMapping.ObjectFields.ObjectField.SourceData }
             *     
             */
            public void setSourceData(ObjectMapping.ObjectFields.ObjectField.SourceData value) {
                this.sourceData = value;
            }

            /**
             * Recupera il valore della proprietà destData.
             * 
             * @return
             *     possible object is
             *     {@link ObjectMapping.ObjectFields.ObjectField.DestData }
             *     
             */
            public ObjectMapping.ObjectFields.ObjectField.DestData getDestData() {
                return destData;
            }

            /**
             * Imposta il valore della proprietà destData.
             * 
             * @param value
             *     allowed object is
             *     {@link ObjectMapping.ObjectFields.ObjectField.DestData }
             *     
             */
            public void setDestData(ObjectMapping.ObjectFields.ObjectField.DestData value) {
                this.destData = value;
            }


            /**
             * <p>Classe Java per anonymous complex type.
             * 
             * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="dest_field" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="dest_label" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "destField",
                "destLabel"
            })
            public static class DestData {

                @XmlElement(name = "dest_field", required = true)
                protected String destField;
                @XmlElement(name = "dest_label", required = true)
                protected String destLabel;

                /**
                 * Recupera il valore della proprietà destField.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDestField() {
                    return destField;
                }

                /**
                 * Imposta il valore della proprietà destField.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDestField(String value) {
                    this.destField = value;
                }

                /**
                 * Recupera il valore della proprietà destLabel.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDestLabel() {
                    return destLabel;
                }

                /**
                 * Imposta il valore della proprietà destLabel.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDestLabel(String value) {
                    this.destLabel = value;
                }

            }


            /**
             * <p>Classe Java per anonymous complex type.
             * 
             * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="source_field" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="source_label" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}byte" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "sourceField",
                "sourceLabel"
            })
            public static class SourceData {

                @XmlElement(name = "source_field", required = true)
                protected String sourceField;
                @XmlElement(name = "source_label", required = true)
                protected String sourceLabel;
                @XmlAttribute(name = "id")
                protected Byte id;

                /**
                 * Recupera il valore della proprietà sourceField.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSourceField() {
                    return sourceField;
                }

                /**
                 * Imposta il valore della proprietà sourceField.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSourceField(String value) {
                    this.sourceField = value;
                }

                /**
                 * Recupera il valore della proprietà sourceLabel.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSourceLabel() {
                    return sourceLabel;
                }

                /**
                 * Imposta il valore della proprietà sourceLabel.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSourceLabel(String value) {
                    this.sourceLabel = value;
                }

                /**
                 * Recupera il valore della proprietà id.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Byte }
                 *     
                 */
                public Byte getId() {
                    return id;
                }

                /**
                 * Imposta il valore della proprietà id.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Byte }
                 *     
                 */
                public void setId(Byte value) {
                    this.id = value;
                }

            }

        }

    }

}
