package it.postel.bean;

import java.sql.Date;

public class ParametriBean {

	private int idClasseModello;
	private String zcode;
	private String descrizione;
	private int  flagLavorato;
	private Date dataLavorazione;
	
	public ParametriBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ParametriBean(int idClasseModello, String zcode, String descrizione, int flagLavorato, Date dataLavorazione) {
		super();
		this.idClasseModello = idClasseModello;
		this.zcode = zcode;
		this.descrizione = descrizione;
		this.flagLavorato = flagLavorato;
		this.dataLavorazione = dataLavorazione;
	}
	
	@Override
	public String toString() {
		return "ParametriBean [idClasseModello=" + idClasseModello + ", zcode=" + zcode + ", descrizione="
				+ descrizione + ", flagLavorato=" + flagLavorato + "]";
	}
	public int getIdClasseModello() {
		return idClasseModello;
	}
	public void setIdClasseModello(int idClasseModello) {
		this.idClasseModello = idClasseModello;
	}
	public String getZcode() {
		return zcode;
	}
	public void setZcode(String zcode) {
		this.zcode = zcode;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public int getFlagLavorato() {
		return flagLavorato;
	}
	public void setFlagLavorato(int flagLavorato) {
		this.flagLavorato = flagLavorato;
	}
	public Date getDataLavorazione() {
		return dataLavorazione;
	}
	public void setDataLavorazione(Date dataLavorazione) {
		this.dataLavorazione = dataLavorazione;
	}
	
	
	
	
}
